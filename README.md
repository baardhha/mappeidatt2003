# Chaos Game

## Overview
The Chaos Game is a JavaFX-based application that allows users to create, visualize, and save fractal images. The application supports both affine transformations and Julia sets, providing flexibility for creating various fractal types.

## Features
- **Create Fractals**: Generate fractal images using affine transformations or Julia sets.
- **Preset Management**: Load and save presets for quick access to frequently used fractal configurations.
- **Zoom In/Out**: Adjust the zoom level of the fractal image.
- **Clear Image**: Clear the current fractal image from the canvas.
- **Save to File**: Save the current fractal configuration to a file.

## Requirements
- **Java Development Kit (JDK) 21 or higher**
- **JavaFX SDK 21 or higher**
- **Apache Maven**

## Getting Started

### Setup
1. **Download and install JDK 21** (if not already installed).
2. **Download and install JavaFX SDK 21** (if not already installed).
3. **Clone the repository**:
   ```bash
   https://gitlab.stud.idi.ntnu.no/baardhha/mappeidatt2003/-/tree/main