package org.example.model;

/**
 * This class represents a Julia transformation applicable to complex numbers.
 * It implements the Transform2D interface, allowing it to be used interchangeably
 * with other Transform2D instances.
 */
public class JuliaTransform implements Transform2D {
  private Complex c;
  private int sign;
  private final double escapeRadius = 2.0;

  /**
   * Constructs a new JuliaTransform.
   *
   * @param c The complex constant in the transformation.
   * @param sign The sign used in the transformation (-1 or +1).
   */
  public JuliaTransform(Complex c, int sign) {
    this.c = c;
    this.sign = sign;
  }

  /**
   * Transforms a given point using the Julia set transformation.
   *
   * @param point The point to transform, which must be an instance of Complex.
   * @return The transformed point as a Complex object, or null if the point escapes.
   * @throws IllegalArgumentException if the point is not an instance of Complex.
   */
  @Override
  public Vector2D transform(Vector2D point) {
    if (!(point instanceof Complex)) {
      throw new IllegalArgumentException("Expected Complex type but received " + point.getClass().getSimpleName());
    }
    Complex z = (Complex) point;
    Complex zNew = z.square().add(c);
    if (zNew.magnitude() > escapeRadius) {
      return null; // Indicates that the point has escaped
    }
    if (sign < 0) {
      zNew = zNew.negate();
    }
    return zNew;
  }

  /**
   * Returns the complex constant used in the transformation.
   *
   * @return The complex constant.
   */
  public Complex getC() {
    return c;
  }

  /**
   * Returns the sign used in the transformation.
   *
   * @return The sign (-1 or +1).
   */
  public int getSign() {
    return sign;
  }

  /**
   * Creates a clone of this JuliaTransform.
   *
   * @return A new JuliaTransform with the same complex constant and sign.
   */
  @Override
  public Transform2D copy() {
    return new JuliaTransform(this.c.clone(), this.sign);
  }
}