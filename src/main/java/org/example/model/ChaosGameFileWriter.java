package org.example.model;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Handles writing configurations for Chaos Game fractals to files.
 */
public class ChaosGameFileWriter {
    /**
     * Writes affine transformation fractal data to a file.
     *
     * @param filePath The path to the file where data will be written.
     * @param minCoords The minimum coordinates of the fractal.
     * @param maxCoords The maximum coordinates of the fractal.
     * @param transforms The list of affine transformations defining the fractal.
     */
    public void writeAffineDataToFile(String filePath, Vector2D minCoords, Vector2D maxCoords, List<Transform2D> transforms) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath))) {
            writer.write("Affine2D\n");
            writer.write(minCoords.getx0() + ", " + minCoords.getx1() + "\n");
            writer.write(maxCoords.getx0() + ", " + maxCoords.getx1() + "\n");
            for (Transform2D transform : transforms) {
                if (transform instanceof AffineTransform2D) {
                    AffineTransform2D affineTransform = (AffineTransform2D) transform;
                    Matrix2x2 matrix = affineTransform.getMatrix();
                    Vector2D vector = affineTransform.getVector();
                    writer.write(matrix.getA00() + ", " + matrix.getA01() + ", " + matrix.getA10() + ", " + matrix.getA11() + ", ");
                    writer.write(vector.getx0() + ", " + vector.getx1() + "\n");
                }
            }
        } catch (IOException e) {
            System.err.println("Error writing to file: " + filePath);
        }
    }

    /**
     * Writes Julia set fractal data to a file.
     *
     * @param filePath The path to the file where data will be written.
     * @param minCoords The minimum coordinates of the fractal.
     * @param maxCoords The maximum coordinates of the fractal.
     * @param juliaConstant The complex constant defining the Julia set fractal.
     */
    public void writeJuliaDataToFile(String filePath, Vector2D minCoords, Vector2D maxCoords, Complex juliaConstant) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath))) {
            writer.write("Julia\n");
            writer.write(minCoords.getx0() + ", " + minCoords.getx1() + "\n");
            writer.write(maxCoords.getx0() + ", " + maxCoords.getx1() + "\n");
            writer.write(juliaConstant.getx0() + ", " + juliaConstant.getx1() + "\n");
        } catch (IOException e) {
            System.err.println("Error writing to file: " + filePath);
        }
    }
}

