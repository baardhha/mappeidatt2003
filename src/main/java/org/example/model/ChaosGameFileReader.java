package org.example.model;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Handles file input and output operations for Chaos Game fractals. This class supports
 * reading and writing configurations for both affine and Julia set fractals.
 */
public class ChaosGameFileReader {
  public Vector2D minCoords;
  public Vector2D maxCoords;
  public List<Transform2D> transforms = new ArrayList<>();
  public Complex juliaConstant;
  private boolean isJuliaTransform = false;

  /**
   * Reads fractal data from a file and parses it into the appropriate data structures.
   *
   * @param filePath The path to the file containing fractal data.
   */
  public void readFromFile(String filePath){
    if (filePath == null) {
      throw new IllegalArgumentException("File path cannot be null");
    }

    resetReader();

    File file = new File(filePath);

    if (!file.exists()) {
      System.err.println("File does not exist: " + filePath);
      return;
    }

    try (Scanner scanner = new Scanner(new File(filePath))) {
      while (scanner.hasNextLine()) {
        String line = scanner.nextLine();
        if (line.startsWith("Affine2D")) {
          readAffineData(scanner);;
        } else if (line.startsWith("Julia")) {
          readJuliaData(scanner);
          isJuliaTransform = true;
        }
      }
    } catch (FileNotFoundException e) {
      System.err.println("Unable to find the file: " + filePath);
    } catch (Exception e) {
      System.err.println("Error reading file: " + filePath + " error " + e.getMessage());
    }
  }

  /**
   * Reads affine transformation data from the scanner.
   *
   * @param scanner The scanner object reading from the fractal data file.
   */
  private void readAffineData(Scanner scanner) {
    if (scanner.hasNextLine()) minCoords = parseVector(scanner.nextLine());
    if (scanner.hasNextLine()) maxCoords = parseVector(scanner.nextLine());

    while (scanner.hasNextLine()) {
      String dataLine = scanner.nextLine();
      if (dataLine.isEmpty() || dataLine.startsWith("#")) {
        continue; // Skip empty lines and comments
      }
      transforms.add(parseAffineTransform(dataLine));
    }
  }

  /**
   * Reads Julia set transformation data from the scanner.
   *
   * @param scanner The scanner object reading from the fractal data file.
   */
  private void readJuliaData(Scanner scanner){
    if (scanner.hasNextLine()) minCoords = parseVector(scanner.nextLine());
    if (scanner.hasNextLine()) maxCoords = parseVector(scanner.nextLine());

    if (scanner.hasNextLine()) {
      String dataLine = scanner.nextLine();
      if (!dataLine.isEmpty() && !dataLine.startsWith("#")) {
        juliaConstant = parseComplex(dataLine);
        transforms.add(new JuliaTransform(juliaConstant, 1));
      }
    }
  }

  /**
   * Parses a vector from a string.
   *
   * @param line The line containing vector data.
   * @return The parsed Vector2D object.
   */
  private Vector2D parseVector(String line) {
    String[] parts = line.split(",");
    return new Vector2D(Double.parseDouble(parts[0].trim()), Double.parseDouble(parts[1].trim()));
  }

  /**
   * Parses an affine transformation from a string.
   *
   * @param line The line containing affine transformation data.
   * @return The parsed Transform2D object.
   */
  private Transform2D parseAffineTransform(String line) {
    String[] parts = line.split(",");
    Matrix2x2 matrix = new Matrix2x2(
            Double.parseDouble(parts[0].trim()), Double.parseDouble(parts[1].trim()),
            Double.parseDouble(parts[2].trim()), Double.parseDouble(parts[3].trim())
    );
    Vector2D vector = new Vector2D(Double.parseDouble(parts[4].trim()), Double.parseDouble(parts[5].trim()));
    return new AffineTransform2D(matrix, vector);
  }

  /**
   * Parses a complex number from a string.
   *
   * @param line The line containing complex number data.
   * @return The parsed Complex object.
   */
  private Complex parseComplex(String line) {
    String[] parts = line.split(",");
    return new Complex(Double.parseDouble(parts[0].trim()), Double.parseDouble(parts[1].trim()));
  }

  /**
   * Resets the reader to its initial state.
   */
  private void resetReader() {
    minCoords = null;
    maxCoords = null;
    transforms.clear();
    juliaConstant = null;
    isJuliaTransform = false;
  }

  public Vector2D getMinCoords() {
    return minCoords;
  }

  public Vector2D getMaxCoords() {
    return maxCoords;
  }

  public List<Transform2D> getTransforms() {
    return transforms;
  }

  /**
   * Determines if the fractal is a Julia set based on the file content.
   *
   * @param filePath The path to the file containing fractal data.
   * @return true if the fractal is a Julia set, false otherwise.
   */
  public boolean isJuliaTransform(String filePath){
    boolean answer = false;

    if (filePath == null) {
      throw new IllegalArgumentException("File path cannot be null");
    }

    resetReader();

    File file = new File(filePath);

    try (Scanner scanner = new Scanner(new File(filePath))) {
      while (scanner.hasNextLine()) {
        String line = scanner.nextLine();
        if (line.startsWith("Affine2D")) {
          readAffineData(scanner);;
        } else if (line.startsWith("Julia")) {
          answer = true;
          readJuliaData(scanner);
          isJuliaTransform = true;
        }
      }
    } catch (FileNotFoundException e) {
      System.err.println("Unable to find the file: " + filePath);
    } catch (Exception e) {
      System.err.println("Error reading file: " + filePath + " error " + e.getMessage());
    }
    return answer;
  }

  public Complex getJuliaConstant() {
    return juliaConstant;
  }
}