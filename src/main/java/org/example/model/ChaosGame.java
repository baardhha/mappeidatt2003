package org.example.model;

import java.util.Random;

/**
 * The ChaosGame class simulates the chaos game, a method of creating fractals
 * using random iterative function systems. This class handles the execution
 * of the game steps, scaling of transformations, and resetting the canvas.
 */
public class ChaosGame {
  private ChaosCanvas canvas;
  private ChaosGameDescription description;
  private Vector2D currentPoint;
  private Random random;

  /**
   * Constructs a ChaosGame with the specified canvas and game description.
   *
   * @param canvas the canvas to draw on
   * @param description the description of the chaos game
   */
  public ChaosGame(ChaosCanvas canvas, ChaosGameDescription description) {
    this.canvas = canvas;
    this.description = description;
    this.currentPoint = description.getTransforms().get(0) instanceof JuliaTransform ? new Complex(0, 0) : new Vector2D(0, 0);
    this.random = new Random();
  }

  /**
   * Returns the canvas used in this chaos game.
   *
   * @return the canvas
   */
  public ChaosCanvas getCanvas() {
    return canvas;
  }

  /**
   * Runs the chaos game for a specified number of steps, applying a zoom factor
   * and a zoom center to the points.
   *
   * @param steps the number of steps to run
   * @param zoomFactor the factor by which to zoom
   * @param zoomCenter the center of the zoom
   */
  public void runSteps(int steps, double zoomFactor, Vector2D zoomCenter) {
    for (int i = 0; i < steps; i++) {
      int transformIndex = random.nextInt(description.getTransforms().size());
      Transform2D transform = description.getTransforms().get(transformIndex);
      Vector2D newPoint = transform.transform(currentPoint);

      if (newPoint == null) {
        continue; // Skip this iteration if the point has escaped
      }

      // Check if the new point is out of bounds
      if (isOutOfBounds(newPoint)) {
        // Skip this point and continue with the next step
        continue;
      }

      currentPoint = newPoint; // Update the current point
      canvas.putPixel(currentPoint, zoomFactor, zoomCenter);
    }
  }

  /**
   * Scales all affine transformations in the game description by the specified factor.
   *
   * @param factor the scaling factor
   */
  public void scaleTransforms(double factor) {
    for (Transform2D transform : description.getTransforms()) {
      if (transform instanceof AffineTransform2D) {
        ((AffineTransform2D) transform).scale(factor);
      }
    }
  }

  /**
   * Resets the chaos game with a new starting point and clears the canvas.
   *
   * @param newStartPoint the new starting point
   */
  public void reset(Vector2D newStartPoint) {
    this.currentPoint = newStartPoint;
    this.canvas.clear();
  }

  /**
   * Prints the current state of the canvas to the console.
   */
  public void printCanvas() {
    canvas.printCanvas();
  }

  /**
   * Checks if a point is out of the bounds of the canvas.
   *
   * @param point the point to check
   * @return true if the point is out of bounds, false otherwise
   */
  private boolean isOutOfBounds(Vector2D point) {
    double x = point.getx0();
    double y = point.getx1();
    return x < canvas.getMinCoords().getx0() || x > canvas.getMaxCoords().getx0() || y < canvas.getMinCoords().getx1() || y > canvas.getMaxCoords().getx1();
  }
}