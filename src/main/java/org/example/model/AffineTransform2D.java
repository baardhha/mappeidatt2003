package org.example.model;

/**
 * The AffineTransform2D class represents a 2D affine transformation.
 * It consists of a 2x2 matrix and a 2D vector. This class allows
 * for transforming 2D points using affine transformations, scaling
 * the transformation, and cloning the transformation.
 */
public class AffineTransform2D implements Transform2D {//Added the implements Transform2D
  private Matrix2x2 matrix;
  private Vector2D vector;

  /**
   * Constructs an AffineTransform2D with the specified matrix and vector.
   *
   * @param matrix the 2x2 matrix for the affine transformation
   * @param vector the 2D vector for the affine transformation
   */
  public AffineTransform2D(Matrix2x2 matrix, Vector2D vector) {
    this.matrix = matrix;
    this.vector = vector;
  }

  /**
   * Returns the matrix of this affine transformation.
   *
   * @return the 2x2 matrix
   */
  public Matrix2x2 getMatrix() {
    return matrix;
  }

  /**
   * Returns the vector of this affine transformation.
   *
   * @return the 2D vector
   */
  public Vector2D getVector() {
    return vector;
  }

  /**
   * Transforms the specified point using this affine transformation.
   *
   * @param point the 2D point to be transformed
   * @return the transformed 2D point
   */
  public Vector2D transform(Vector2D point){
    Vector2D transformedPoint = matrix.multiply(point);
    return transformedPoint.add(vector);
  }

  /**
   * Scales the affine transformation by the specified factor.
   *
   * @param factor the scaling factor
   */
  public void scale(double factor) {
    Matrix2x2 scalingMatrix = new Matrix2x2(
            factor, 0,
            0, factor
    );
    this.matrix = scalingMatrix.multiply(this.matrix);
  }

  /**
   * Creates and returns a copy of this affine transformation.
   *
   * @return a clone of this instance
   */
  @Override
  public AffineTransform2D copy() {
    return new AffineTransform2D(this.matrix.copy(), this.vector.copy());
  }

  /**
   * Returns a string representation of this affine transformation.
   *
   * @return a string representation of this object
   */
  @Override
  public String toString() {
    return "AffineTransform2D{" +
            "matrix=" + matrix +
            ", vector=" + vector +
            '}';
  }
}
