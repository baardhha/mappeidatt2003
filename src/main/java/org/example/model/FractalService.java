package org.example.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * The FractalService class provides methods to read a fractal description from a file
 * and create a ChaosGameDescription object. It utilizes ChaosGameFileReader to read the file.
 */
public class FractalService {
    private static final String PRESET_DIRECTORY = "src/main/resources/presets/";

    /**
     * Retrieves the list of preset files from the preset directory.
     *
     * @return A list of file paths to the preset files.
     */
    public List<String> getPresetFiles() {
        File directory = new File(PRESET_DIRECTORY);
        List<String> presetFiles = new ArrayList<>();

        if (directory.exists() && directory.isDirectory()) {
            File[] files = directory.listFiles((dir, name) -> name.endsWith(".txt"));
            if (files != null) {
                for (File file : files) {
                    presetFiles.add(file.getAbsolutePath());
                }
            }
        }
        return presetFiles;
    }

    /**
     * Reads a fractal description from a file and creates a ChaosGameDescription object.
     *
     * @param filePath The path to the file containing fractal data.
     * @return A ChaosGameDescription object containing the fractal data, or null if the filePath is null.
     */
    public ChaosGameDescription getDescription(String filePath) {
        if (filePath == null) {
            return null;  // Handle null filePath appropriately
        }
        ChaosGameFileReader reader = new ChaosGameFileReader();
        reader.readFromFile(filePath);
        return new ChaosGameDescription(reader.getTransforms(), reader.getMinCoords(), reader.getMaxCoords(), reader.getJuliaConstant());
    }
}
