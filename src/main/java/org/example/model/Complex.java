package org.example.model;

/**
 * The Complex class represents a complex number and extends the Vector2D class.
 * It provides methods for complex number arithmetic, including square root,
 * squaring, negation, addition, and magnitude calculation.
 */
public class Complex extends Vector2D {

  /**
   * Constructs a Complex number with the specified real and imaginary parts.
   *
   * @param realPart the real part of the complex number
   * @param imaginaryPart the imaginary part of the complex number
   */
  public Complex  (double realPart, double imaginaryPart) {
    super(realPart, imaginaryPart);
  }

  /**
   * Returns the square root of this complex number.
   *
   * @return the square root of this complex number
   */
  public Complex sqrt() {
    double x = getx0();
    double y = getx1();
    double r = Math.sqrt(x * x + y * y);

    double realPart = Math.sqrt((r + x) / 2);
    double imaginaryPart;
    if (y ==  0 && x < 0) {
      imaginaryPart = Math.sqrt((r - x) / 2);
    } else {
      imaginaryPart = Math.signum(y) * Math.sqrt((r - x) / 2);
    }

    return new Complex(realPart, imaginaryPart);
  }

  /**
   * Returns the square of this complex number.
   *
   * @return the square of this complex number
   */
  public Complex square() {
    double real = getx0() * getx0() - getx1() * getx1();
    double imag = 2 * getx0() * getx1();
    return new Complex(real, imag);
  }

  /**
   * Returns the negation of this complex number.
   *
   * @return the negation of this complex number
   */
  public Complex negate() {
    return new Complex(-getx0(), -getx1());
  }

  /**
   * Returns the magnitude of this complex number.
   *
   * @return the magnitude of this complex number
   */
  public double magnitude() {
    return Math.sqrt(getx0() * getx0() + getx1() * getx1());
  }

  /**
   * Adds the specified vector to this complex number and returns the result.
   *
   * @param other the vector to add
   * @return the result of adding the specified vector to this complex number
   */
  public Complex add(Vector2D other) {
    return new Complex(getx0() + other.getx0(), getx1() + other.getx1());
  }

  /**
   * Creates and returns a copy of this complex number.
   *
   * @return a clone of this complex number
   */
  @Override
  public Complex clone() {
    return new Complex(this.getx0(), this.getx1());
  }

  /**
   * Returns a string representation of this complex number.
   *
   * @return a string representation of this complex number
   */
  @Override
  public String toString() {
    return "Complex{" +
            "real=" + getx0() +
            ", imaginary=" + getx1() +
            '}';
  }
}