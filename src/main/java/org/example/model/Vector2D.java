package org.example.model;

import java.util.Objects;

/**
 * The Vector2D class represents a two-dimensional vector and provides methods for vector operations,
 * including addition, subtraction, cloning, and comparison.
 */
public class Vector2D {
  private double x0;
  private double x1;

  /**
   * Constructs a new Vector2D with the specified coordinates.
   *
   * @param x0 The x-coordinate of the vector.
   * @param x1 The y-coordinate of the vector.
   */
  public Vector2D(double x0, double x1) {
    this.x0 = x0;
    this.x1 = x1;
  }

  /**
   * Returns the x-coordinate of the vector.
   *
   * @return The x-coordinate.
   */
  public double getx0() {
    return x0;
  }

  /**
   * Returns the y-coordinate of the vector.
   *
   * @return The y-coordinate.
   */
  public double getx1() {
    return x1;
  }

  /**
   * Adds another vector to this vector.
   *
   * @param other The vector to add.
   * @return A new Vector2D representing the result of the addition.
   */
  public Vector2D add(Vector2D other) {
    return new Vector2D(x0 + other.x0, x1 + other.x1);
  }


  /**
   * Subtracts another vector from this vector.
   *
   * @param other The vector to subtract.
   * @return A new Vector2D representing the result of the subtraction.
   */
  public Vector2D subtract(Vector2D other) {
    return new Vector2D(x0 - other.x0, x1 - other.x1);
  }

  /**
   * Checks if this vector is equal to another object.
   *
   * @param o The object to compare to.
   * @return true if the object is a Vector2D with the same coordinates, false otherwise.
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Vector2D)) return false;
    Vector2D vector2D = (Vector2D) o;
    return Double.compare(vector2D.x0, x0) == 0 &&
        Double.compare(vector2D.x1, x1) == 0;
  }

  /**
   * Creates a clone of this vector.
   *
   * @return A new Vector2D with the same coordinates.
   */

  public Vector2D copy() {
    return new Vector2D(this.x0, this.x1);
  }
  /**
   * Returns the hash code of this vector.
   *
   * @return The hash code.
   */
  @Override
  public int hashCode() {
    return Objects.hash(x0, x1);
  }

  /**
   * Returns a string representation of this vector.
   *
   * @return A string representing the vector.
   */
  @Override
  public String toString() {
    return "Vector2D{" +
            "x0=" + x0 +
            ", x1=" + x1 +
            '}';
  }
}
