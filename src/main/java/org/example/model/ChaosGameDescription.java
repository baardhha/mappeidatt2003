package org.example.model;

import java.util.ArrayList;
import java.util.List;

/**
 * The ChaosGameDescription class encapsulates the description of a chaos game.
 * It includes the transformations, coordinate bounds, and an optional Julia constant.
 */
public class ChaosGameDescription {
  private Vector2D minCoords;
  private Vector2D maxCoords;
  private List<Transform2D> transforms;
  Complex juliaConstant;//Gjorde denne public for å kunne teste den

  /**
   * Constructs a ChaosGameDescription with the specified transformations, coordinate bounds,
   * and an optional Julia constant for Julia set transformations.
   *
   * @param transforms the list of transformations to be used in the chaos game
   * @param minCoords the minimum coordinates for the transformations
   * @param maxCoords the maximum coordinates for the transformations
   * @param juliaConstant the Julia constant for Julia set transformations (can be null)
   */
  public ChaosGameDescription(List<Transform2D> transforms, Vector2D minCoords, Vector2D maxCoords, Complex juliaConstant) {
    this.transforms = transforms;
    this.minCoords = minCoords;
    this.maxCoords = maxCoords;
    this.juliaConstant = juliaConstant;
  }

  /**
   * Constructs a ChaosGameDescription with the specified transformations and coordinate bounds.
   * This constructor is provided for backward compatibility.
   *
   * @param transforms the list of transformations to be used in the chaos game
   * @param minCoords the minimum coordinates for the transformations
   * @param maxCoords the maximum coordinates for the transformations
   */
  public ChaosGameDescription(List<Transform2D> transforms, Vector2D minCoords, Vector2D maxCoords) {
    this(transforms, minCoords, maxCoords, null); // Call the primary constructor with null for the Julia constant
  }

  /**
   * Returns the list of transformations used in the chaos game.
   *
   * @return the list of transformations
   */
  public List<Transform2D> getTransforms(){
    return transforms;
  }

  /**
   * Returns the minimum coordinates for the transformations.
   *
   * @return the minimum coordinates
   */
  public Vector2D getMinCoords(){
    return minCoords;
  }

  /**
   * Returns the maximum coordinates for the transformations.
   *
   * @return the maximum coordinates
   */
  public Vector2D getMaxCoords(){
    return maxCoords;
  }

  /**
   * Sets the list of transformations used in the chaos game.
   *
   * @param transforms the new list of transformations
   */
  public void setTransforms(List<Transform2D> transforms){
    this.transforms = transforms;
  }

  /**
   * Sets the Julia constant for Julia set transformations.
   *
   * @param juliaConstant the new Julia constant
   */
  public void setJuliaConstant(Complex juliaConstant){
    this.juliaConstant = juliaConstant;
  }

  /**
   * Creates and returns a copy of this ChaosGameDescription.
   *
   * @return a clone of this instance
   */

  public ChaosGameDescription copy() {
    List<Transform2D> tansformsCopy = new ArrayList<>();
    for (Transform2D transform : transforms) {
      tansformsCopy.add(transform.copy());
    }
    return new ChaosGameDescription(tansformsCopy, this.minCoords.copy(), this.maxCoords.copy(), this.juliaConstant != null ? this.juliaConstant.clone() : null);
  }
}
