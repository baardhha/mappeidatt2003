package org.example.model;

import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

/**
 * The ChaosCanvas class represents a canvas for drawing fractals or other chaotic
 * systems. It provides methods for transforming coordinates, updating the transformation,
 * and zooming into specific areas of the canvas.
 */
public class ChaosCanvas {
  private int[][] canvas;
  private int width;
  private int height;
  private Vector2D minCoords;
  private Vector2D maxCoords;
  AffineTransform2D transformCoordsToIndices;//Changed to public for testing purposes

  /**
   * Constructs a ChaosCanvas with the specified dimensions and coordinate bounds.
   *
   * @param width the width of the canvas in pixels
   * @param height the height of the canvas in pixels
   * @param minCoords the minimum coordinates for the transformation
   * @param maxCoords the maximum coordinates for the transformation
   */
  public ChaosCanvas (int width, int height, Vector2D minCoords, Vector2D maxCoords) {
    this.width = width;
    this.height = height;
    this.minCoords = minCoords;
    this.maxCoords = maxCoords;
    this.canvas = new int[height][width];
    this.transformCoordsToIndices = createTransform(minCoords, maxCoords, width, height);
  }

  /**
   * Creates a transformation from world coordinates to canvas indices.
   *
   * @param minCoords the minimum coordinates
   * @param maxCoords the maximum coordinates
   * @param width the width of the canvas
   * @param height the height of the canvas
   * @return the affine transformation
   */
  private AffineTransform2D createTransform(Vector2D minCoords, Vector2D maxCoords, int width, int height) {
    if (minCoords.getx0() >= maxCoords.getx0() || minCoords.getx1() >= maxCoords.getx1()) {
      throw new IllegalArgumentException("Invalid coordinates: minCoords must be less than maxCoords.");
    }
    double scaleX = (width - 1) / (maxCoords.getx0() - minCoords.getx0());
    double scaleY = (height - 1) / (maxCoords.getx1() - minCoords.getx1());
    double translateX = -minCoords.getx0() * scaleX;
    double translateY = -minCoords.getx1() * scaleY;

    Matrix2x2 matrix = new Matrix2x2(scaleX, 0, 0, scaleY);
    Vector2D vector = new Vector2D(translateX, translateY);

    return new AffineTransform2D(matrix, vector);
  }

  /**
   * Updates the transformation based on new minimum and maximum coordinates.
   *
   * @param minCoords the new minimum coordinates
   * @param maxCoords the new maximum coordinates
   */
  public void updateTransform(Vector2D minCoords, Vector2D maxCoords) {
    this.minCoords = minCoords;
    this.maxCoords = maxCoords;
    this.transformCoordsToIndices = createTransform(minCoords, maxCoords, width, height);
  }

  /**
   * Gets the value of the pixel at the specified point.
   *
   * @param point the 2D point in world coordinates
   * @return the value of the pixel (1 for set, 0 for unset)
   */
  public int getPixel(Vector2D point) {
    Vector2D transformedPoint = transformCoordsToIndices.transform(point);
    int i = (int) transformedPoint.getx1();
    int j = (int) transformedPoint.getx0();
    if (i >= 0 && i < height && j >= 0 && j < width) {
      return canvas[i][j];
    }
    return 0; // Or throw an exception, based on your application needs
  }

  /**
   * Sets a pixel on the canvas at the specified point, considering zoom factor and zoom center.
   *
   * @param point the 2D point in world coordinates
   * @param zoomFactor the factor by which to zoom
   * @param zoomCenter the center of the zoom
   */
  public void putPixel(Vector2D point, double zoomFactor, Vector2D zoomCenter) {
    Vector2D transformedPoint = transformCoordsToIndices.transform(point);

    // Calculate the offset based on the zoom center
    double offsetX = (transformedPoint.getx0() - zoomCenter.getx0()) * zoomFactor;
    double offsetY = (transformedPoint.getx1() - zoomCenter.getx1()) * zoomFactor;

    // Apply the offset to the zoom center
    int i = (int) Math.round(zoomCenter.getx1() + offsetY);
    int j = (int) Math.round(zoomCenter.getx0() + offsetX);

    // Debugging output
    System.out.println("Original point: " + point);
    System.out.println("Transformed point: " + transformedPoint);
    System.out.println("Offset: (" + offsetX + ", " + offsetY + ")");
    System.out.println("Canvas indices: (i=" + i + ", j=" + j + ")");
    System.out.println("Canvas bounds: width=" + width + ", height=" + height);

    // Check if the coordinates are within the canvas bounds
    if (i >= 0 && i < height && j >= 0 && j < width) {
      canvas[i][j] = 1;
      System.out.println("Pixel set at (i=" + i + ", j=" + j + ")");
    } else {
      System.out.println("Point out of bounds: " + point + " -> " + transformedPoint);
    }
  }


  /**
   * Returns the 2D array representing the canvas.
   *
   * @return the canvas as a 2D array
   */
  public int[][] getCanvasArray() {
    return canvas;
  }

  /**
   * Clears the canvas by setting all pixels to 0.
   */
  public void clear() {
    for (int i = 0; i < height; i++) {
      for (int j = 0; j < width; j++) {
        canvas[i][j] = 0;
      }
    }
  }

  /**
   * Converts the canvas to a WritableImage for rendering.
   *
   * @return the WritableImage representing the canvas
   */
  public WritableImage toWritableImage() {
    WritableImage image = new WritableImage(width, height);
    PixelWriter writer = image.getPixelWriter();

    // Loop through each pixel on the canvas
    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        // Set the color based on the value in the canvas array
        if (canvas[y][x] == 1) {
          writer.setColor(x, y, Color.WHITE); // Black for fractal points
        } else {
          writer.setColor(x, y, Color.TRANSPARENT); // White for background
        }
      }
    }
    return image;
  }

  /**
   * Prints the canvas to the console.
   * Uses Unicode block characters for filled pixels.
   */
  public void printCanvas() {
    for (int i = 0; i < height; i++) {
      for (int j = 0; j < width; j++) {
        System.out.print(canvas[i][j] == 1 ? "\u2588" : " "); // Print '*' for 1, ' ' for 0
      }
      System.out.println(); // Newline after each row
    }
  }


  public void printCanvasDebug() {
    for (int i = 0; i < height; i++) {
      for (int j = 0; j < width; j++) {
        System.out.print(canvas[i][j] == 1 ? "*" : " "); // Print '*' for 1, ' ' for 0
      }
      System.out.println(); // Newline after each row
    }
  }

  /**
   * Returns the minimum coordinates of the canvas.
   *
   * @return the minimum coordinates as a Vector2D
   */
  public Vector2D getMinCoords() {
    return minCoords;
  }

  /**
   * Returns the maximum coordinates of the canvas.
   *
   * @return the maximum coordinates as a Vector2D
   */
  public Vector2D getMaxCoords() {
    return maxCoords;
  }

  /**
   * Returns the width of the canvas.
   *
   * @return the width of the canvas
   */
  public int getWidth() {
    return width;
  }

  /**
   * Returns the height of the canvas.
   *
   * @return the height of the canvas
   */
  public int getHeight() {
    return height;
  }
}