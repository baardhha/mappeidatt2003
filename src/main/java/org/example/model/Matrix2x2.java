package org.example.model;

import java.util.Objects;

/**
 * The Matrix2x2 class represents a 2x2 matrix and provides methods for matrix operations,
 * including matrix-vector multiplication, matrix-matrix multiplication, and cloning.
 */
public class Matrix2x2 {
  private double a00;
  private double a01;
  private double a10;
  private double a11;

  /**
   * Constructs a new Matrix2x2 with the specified elements.
   *
   * @param a00 The element at row 0, column 0.
   * @param a01 The element at row 0, column 1.
   * @param a10 The element at row 1, column 0.
   * @param a11 The element at row 1, column 1.
   */
  public Matrix2x2(double a00, double a01, double a10, double a11) {
    this.a00 = a00;
    this.a01 = a01;
    this.a10 = a10;
    this.a11 = a11;
  }

  /**
   * Returns the element at row 0, column 0.
   *
   * @return The element at row 0, column 0.
   */
  public double getA00() {
    return a00;
  }

  /**
   * Returns the element at row 0, column 1.
   *
   * @return The element at row 0, column 1.
   */
  public double getA01() {
    return a01;
  }

  /**
   * Returns the element at row 1, column 0.
   *
   * @return The element at row 1, column 0.
   */
  public double getA10() {
    return a10;
  }

  /**
   * Returns the element at row 1, column 1.
   *
   * @return The element at row 1, column 1.
   */
  public double getA11() {
    return a11;
  }

  /**
   * Multiplies this matrix by a vector.
   *
   * @param v The vector to multiply.
   * @return The resulting vector from the matrix-vector multiplication.
   */
  public Vector2D multiply(Vector2D v) {
    double newX = a00 * v.getx0() + a01 * v.getx1();
    double newY = a10 * v.getx0() + a11 * v.getx1();
    return new Vector2D(newX, newY);
  }

  /**
   * Multiplies this matrix by another matrix.
   *
   * @param other The other matrix to multiply.
   * @return The resulting matrix from the matrix-matrix multiplication.
   */
  public Matrix2x2 multiply(Matrix2x2 other) {
    double newA00 = this.a00 * other.a00 + this.a01 * other.a10;
    double newA01 = this.a00 * other.a01 + this.a01 * other.a11;
    double newA10 = this.a10 * other.a00 + this.a11 * other.a10;
    double newA11 = this.a10 * other.a01 + this.a11 * other.a11;
    return new Matrix2x2(newA00, newA01, newA10, newA11);
  }

  /**
   * Creates a clone of this Matrix2x2.
   *
   * @return A new Matrix2x2 with the same elements.
   */

  public Matrix2x2 copy() {
    return new Matrix2x2(this.a00, this.a01, this.a10, this.a11);
  }

  @Override
  public boolean equals(Object o) { //
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Matrix2x2 matrix2x2 = (Matrix2x2) o;
    return Double.compare(matrix2x2.a00, a00) == 0 &&
            Double.compare(matrix2x2.a01, a01) == 0 &&
            Double.compare(matrix2x2.a10, a10) == 0 &&
            Double.compare(matrix2x2.a11, a11) == 0;
  }

  @Override
  public int hashCode() {
    return Objects.hash(a00, a01, a10, a11);
  }

  /**
   * Returns a string representation of this matrix.
   *
   * @return A string representing the matrix.
   */
  @Override
  public String toString() {
    return "Matrix2x2{" +
            "a00=" + a00 +
            ", a01=" + a01 +
            ", a10=" + a10 +
            ", a11=" + a11 +
            '}';
  }
}
