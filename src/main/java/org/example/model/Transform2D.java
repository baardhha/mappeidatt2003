package org.example.model;

/**
 * The Transform2D interface represents a 2D transformation.
 * It provides methods for transforming a point and cloning the transformation.
 */
public interface Transform2D {

  /**
   * Transforms a given point.
   *
   * @param point The point to transform.
   * @return The transformed point.
   */
  Vector2D transform(Vector2D point);

  /**
   * Creates a clone of this transformation.
   *
   * @return A new instance of the transformation.
   */
  Transform2D copy();//Added the Transform2D return type
}
