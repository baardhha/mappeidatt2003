package org.example.controller;

import javafx.scene.control.Alert;
import javafx.stage.Stage;
import org.example.model.*;
import org.example.view.FractalDetailBox;
import org.example.view.Game;
import org.example.view.InputBox;
import org.example.view.MainMenu;
import org.example.view.ConfirmBox;

import java.io.File;

import java.util.ArrayList;
import java.util.List;

/**
 * Controller class for the Chaos Game application.
 * Manages interactions between the main menu, game view, and fractal details.
 */
public class ChaosGameController {
  private MainMenu mainMenu;
  private Game game;
  private Stage primaryStage;
  private FractalService fractalService;
  private FractalDetailBox fractalDetailBox;
  private double currentZoomFactor = 1.0;
  private String presetFilePath = "src/main/resources/presets/";
  private List<String> presetFilePaths = new ArrayList<>();
  private List<ChaosGameDescription> presetDescriptions = new ArrayList<>();
  private Vector2D zoomCenter;
  private ChaosGameFileWriter chaosGameFileWriter = new ChaosGameFileWriter();

  /**
   * Constructor for the ChaosGameController class.
   * Initializes the controller with references to the main menu, game view, and primary stage.
   * @param mainMenu The main menu view.
   * @param game The game view.
   * @param primaryStage The primary stage of the application.
   */
  public ChaosGameController(MainMenu mainMenu, Game game, Stage primaryStage){
    this.mainMenu = mainMenu;
    this.game = game;
    this.primaryStage = primaryStage;
    this.fractalService = new FractalService();
    this.fractalDetailBox = new FractalDetailBox(ChaosGameController.this);

    setupHandlers();

    loadPresets();

    zoomCenter = new Vector2D(primaryStage.getWidth() / 2, primaryStage.getHeight() / 2);
  }

  /**
   * Sets up event handlers for the UI components.
   */
  private void setupHandlers() {
    mainMenu.getStartGameButton().setOnAction(e -> handleStartGame());
    mainMenu.getExitGameButton().setOnAction(e -> handleExit());

    game.getBackToMainMenuButton().setOnAction(e -> handleBackToMainMenu());
    game.getNewFractalButton().setOnAction(e -> handleNewFractalButton());
    game.getEditFractalButton().setOnAction(e -> handleEditFractal());
    game.getClearButton().setOnAction(e -> handleClearButton());
    game.getZoomInButton().setOnAction(e -> {
      if (game.getFractalImageView().getImage() != null){
        currentZoomFactor += 0.1;
        handleCreateButton();
      }
    });
    game.getZoomOutButton().setOnAction(e -> {
      if (game.getFractalImageView().getImage() != null){
        currentZoomFactor = Math.max(0.1, currentZoomFactor - 0.1);
        handleCreateButton();
      }
    });
    game.getSaveToFileButton().setOnAction(e -> handleSaveToFile());

    fractalDetailBox.getCreateButton().setOnAction(e -> handleCreateButton());
    fractalDetailBox.getClearButton().setOnAction(e -> fractalDetailBox.displayNoPreset());

    primaryStage.setOnCloseRequest(e -> {
      e.consume();
      handleExit();
    });
  }

  /**
   * Loads preset fractals from the preset directory.
   */
  private void loadPresets() {
    File presetDirectory = new File(presetFilePath);
    if (presetDirectory.exists() && presetDirectory.isDirectory()) {
      File[] presetFiles = presetDirectory.listFiles((dir, name) -> name.endsWith(".txt"));
      if (presetFiles != null) {
        for (File file : presetFiles) {
          String filePath = file.getPath();
          if (filePath != null) {
            presetFilePaths.add(filePath);
            ChaosGameDescription description = fractalService.getDescription(filePath);
            if (description != null) {
              description = description.copy();
              presetDescriptions.add(description);
            } else {
              System.err.println("Error loading preset: " + filePath);
            }
          }
        }
      } else {
        System.err.println("No preset files found in directory: " + presetFilePath);
      }
    } else {
      System.err.println("Preset directory not found: " + presetFilePath);
    }
    fractalDetailBox.updatePresetChoiceBox(presetFilePaths);
  }

  /**
   * Handles the action of starting the game.
   */
  private void handleStartGame() {
    primaryStage.setScene(game.getScene());
  }

  /**
   * Handles the action of exiting the game.
   */
  private void handleExit() {
    boolean answer = ConfirmBox.display("Exit Game", "Are you sure you want to exit the game?");
    if (answer) {
      System.out.println("Exiting the game...");
      primaryStage.close();
    } else {
      System.out.println("Not exiting the game...");
    }
  }

  /**
   * Handles the action of returning to the main menu.
   */
  private void handleBackToMainMenu() {
    primaryStage.setScene(mainMenu.getScene());
  }

  /**
   * Clears the current fractal image.
   */
  public void handleClearButton() {
    game.getFractalImageView().setImage(null);
    currentZoomFactor = 1.0;
  }

  /**
   * Handles the action of creating a new fractal.
   */
  public void handleNewFractalButton() {
    fractalDetailBox.displayEmpty();
  }

  /**
   * Handles the action of editing the current fractal.
   */
  public void handleEditFractal() {
    ChaosGameDescription currentDescription = getCurrentFractalDescription();
    if (currentDescription.getTransforms() == null && currentDescription.getMinCoords() == null && currentDescription.getMaxCoords() == null) {
      noFractalToEdit();
    } else {
      fractalDetailBox.displayWithValues(currentDescription);
    }
  }

  /**
   * Retrieves the current fractal description from the fractal detail box.
   * @return The current fractal description.
   */
  private ChaosGameDescription getCurrentFractalDescription() {
    try {
      List<Transform2D> transforms = fractalDetailBox.getCurrentTransforms();
      Vector2D minCoords = new Vector2D(fractalDetailBox.getMinXField(), fractalDetailBox.getMinYField());
      Vector2D maxCoords = new Vector2D(fractalDetailBox.getMaxXField(), fractalDetailBox.getMaxYField());

      return new ChaosGameDescription(transforms, minCoords, maxCoords);
      }
    catch (NumberFormatException e) {
      return new ChaosGameDescription(null, null, null);
    }
  }

  /**
   * Handles the action of creating a fractal based on the details provided.
   */
  public void handleCreateButton() {;
    int width = (int) primaryStage.getWidth() - 100;
    int height = (int) primaryStage.getHeight() - 100;
    Vector2D minCoords = getCurrentFractalDescription().getMinCoords();
    Vector2D maxCoords = getCurrentFractalDescription().getMaxCoords();

    int iterations = fractalDetailBox.getIterations();
    Vector2D zoomCenter = new Vector2D(width/2, height/2);

    ChaosCanvas canvas = new ChaosCanvas(width, height, minCoords, maxCoords);
    ChaosGameDescription description = getCurrentFractalDescription();

    ChaosGame chaosGame = new ChaosGame(canvas, description);
    chaosGame.runSteps(iterations, currentZoomFactor, zoomCenter);

    game.getFractalImageView().setImage(canvas.toWritableImage());
    fractalDetailBox.getWindow().close();
  }

  /**
   * Handles the action of saving the current fractal to a file.
   */
  public void handleSaveToFile() {
    ChaosGameDescription description = getCurrentFractalDescription();

    if (description.getTransforms() == null && description.getMinCoords() == null && description.getMaxCoords() == null){
      noFractalToSave();
    } else {
      String saveFileName = InputBox.display("Save to file", "Enter the file name:");
      //File file = new File(presetFilePath + saveFileName + ".txt");

      if (saveFileName == null || saveFileName.trim().isEmpty()) {
        noFileEntered();
      } else {
        File file = new File(presetFilePath + saveFileName + ".txt");
        if (fractalDetailBox.getTransformationChoice() == "Affine") {
          if (file.exists()) {
            fileAlreadyExists();
          } else {
            chaosGameFileWriter.writeAffineDataToFile(presetFilePath + saveFileName + ".txt", description.getMinCoords(), description.getMaxCoords(), description.getTransforms());
            successfulSave();
          }
        } else if (fractalDetailBox.getTransformationChoice() == "Julia") {
          if (file.exists()) {
            fileAlreadyExists();
          } else {
            Complex juliaConstant = fractalDetailBox.getJuliaConstant();
            chaosGameFileWriter.writeJuliaDataToFile(presetFilePath + saveFileName + ".txt", description.getMinCoords(), description.getMaxCoords(), fractalDetailBox.getJuliaConstant());
            successfulSave();
          }
        }
      }
    }
  }

  /**
   * Displays an alert indicating a successful save.
   */
  private void successfulSave() {
    Alert alert = new Alert(Alert.AlertType.INFORMATION);
    alert.setTitle("Information");
    alert.setHeaderText("Fractal saved to file");
    alert.showAndWait();
  }

  /**
   * Displays an alert indicating that the file already exists.
   */
  private void fileAlreadyExists() {
    Alert alert = new Alert(Alert.AlertType.ERROR);
    alert.setTitle("Error");
    alert.setHeaderText("File already exists");
    alert.showAndWait();
  }

  /**
   * Displays an alert indicating that no file name was entered.
   */
  private void noFileEntered() {
    Alert alert = new Alert(Alert.AlertType.ERROR);
    alert.setTitle("Error");
    alert.setHeaderText("No file name entered");
    alert.showAndWait();
  }

  /**
   * Displays an alert indicating that there is no fractal to save.
   */
  private void noFractalToSave() {
    Alert alert = new Alert(Alert.AlertType.ERROR);
    alert.setTitle("Error");
    alert.setHeaderText("No fractal to save");
    alert.showAndWait();
  }

  /**
   * Displays an alert indicating that there is no fractal to edit.
   */
  private void noFractalToEdit() {
    Alert alert = new Alert(Alert.AlertType.ERROR);
    alert.setTitle("Error");
    alert.setHeaderText("No fractal to edit");
    alert.showAndWait();
  }

  /**
   * Fills the fractal detail box with the details of the selected preset.
   * @param filePath The file path of the selected preset.
   */
  public void fillPresetDetails(String filePath) {
    if (filePath != null) {  // Add null check here
      ChaosGameDescription description = fractalService.getDescription(filePath);
      if (description != null) {
        description = description.copy();
        fractalDetailBox.updateFields(description);
      } else {
        System.err.println("Error loading preset details: " + filePath);
      }
    } else {
      System.err.println("Error: filePath is null");
    }
  }
}
