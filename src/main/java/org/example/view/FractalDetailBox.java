package org.example.view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.example.controller.ChaosGameController;
import org.example.model.*;
import org.example.model.AffineTransform2D;

import java.util.ArrayList;
import java.util.List;

/**
 * A class for displaying and managing the fractal detail input box.
 */
public class FractalDetailBox {
  private Stage window;
  private ChoiceBox<String> presetChoiceBox;
  private ChoiceBox<String> transformChoiceBox;
  private TextField minXField;
  private TextField minYField;
  private TextField maxXField;
  private TextField maxYField;
  private VBox affineTransformationInputs;
  private VBox juliaTransformationInputs;
  private VBox affineTransformationsContainer;
  private ScrollPane affineTransformationsScrollPane;
  private TextField iterationsField;
  private Button createButton;
  private Button clearButton;
  private ChaosGameController chaosGameController;
  private String selectedPreset;
  private ChaosGameFileReader chaosGameFileReader;

  /**
   * Constructor for FractalDetailBox.
   *
   * @param chaosGameController the controller for handling chaos game logic.
   */
  public FractalDetailBox(ChaosGameController chaosGameController) {
    this.chaosGameController = chaosGameController;
    window = new Stage();
    window.initModality(Modality.APPLICATION_MODAL);
    window.setTitle("Fractal Details");

    createButton = new Button("Create");
    clearButton = new Button("Clear");

    presetChoiceBox = new ChoiceBox<>();
    presetChoiceBox.setItems(FXCollections.observableArrayList("Custom"));
    presetChoiceBox.setValue("Custom");

    transformChoiceBox = new ChoiceBox<>();
    transformChoiceBox.getItems().addAll("Affine", "Julia");
    transformChoiceBox.setValue("Affine");

    affineTransformationInputs = createAffineTransformationInputs();
    juliaTransformationInputs = createJuliaTransformationInputs();

    presetChoiceBox.setOnAction(e -> {
      selectedPreset = presetChoiceBox.getValue();
      selectedPreset = presetChoiceBox.getValue();
      if (selectedPreset != null && !selectedPreset.equals("Custom")) {
        displayPreset();
        chaosGameController.fillPresetDetails(selectedPreset);
      }
    });

    iterationsField = new TextField();

    chaosGameFileReader = new ChaosGameFileReader();
  }

  /**
   * Updates the preset choice box with a list of preset file paths.
   *
   * @param presetFilePaths the list of preset file paths.
   */
  public void updatePresetChoiceBox(List<String> presetFilePaths) {
    ObservableList<String> items = FXCollections.observableArrayList(presetFilePaths);
    items.add(0,"Custom");
    presetChoiceBox.setItems(items);
  }

  /**
   * Displays the fractal detail input box with the given description.
   *
   * @param description the chaos game description.
   */
  public void display(ChaosGameDescription description) {
    GridPane layout = new GridPane();
    layout.setPadding(new Insets(10));
    layout.setVgap(8);
    layout.setHgap(10);
    layout.setAlignment(Pos.CENTER);

    Label presetLabel = new Label("Presets:");

    Label transformLabel = new Label("Transform:");

    transformChoiceBox.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> {
      if ("Affine".equals(newValue)) {
        affineTransformationInputs.setVisible(true);
        juliaTransformationInputs.setVisible(false);
      } else if ("Julia".equals(newValue)) {
        affineTransformationInputs.setVisible(false);
        juliaTransformationInputs.setVisible(true);
      }
    });

    Label iterationsLabel = new Label("Iterations:");
    iterationsField.setPromptText("Number of iterations");

    Label minXAndYLabel = new Label("Min X and Y:");
    minXField = new TextField();
    minXField.setPromptText("Min X");
    minYField = new TextField();
    minYField.setPromptText("Min Y");

    Label maxXAndYLabel = new Label("Max X and Y:");
    maxXField = new TextField();
    maxXField.setPromptText("Max X");
    maxYField = new TextField();
    maxYField.setPromptText("Max Y");

    if (description != null) {
      updateFields(description);
    }

    layout.add(presetLabel, 0, 0);
    layout.add(presetChoiceBox, 1, 0);
    layout.add(transformLabel, 0, 1);
    layout.add(transformChoiceBox, 1, 1);
    layout.add(minXAndYLabel, 0, 2);
    layout.add(minXField, 1, 2);
    layout.add(minYField, 2, 2);
    layout.add(maxXAndYLabel, 0, 3);
    layout.add(maxXField, 1, 3);
    layout.add(maxYField, 2, 3);

    layout.add(affineTransformationInputs, 0, 4);
    layout.setColumnSpan(affineTransformationInputs, 3);
    layout.add(juliaTransformationInputs, 0, 4);
    layout.setColumnSpan(juliaTransformationInputs, 3);

    layout.add(iterationsLabel, 0, 5);
    layout.add(iterationsField, 1, 5);
    layout.add(createButton, 1, 6);
    layout.add(clearButton, 2, 6);

    Scene scene = new Scene(layout);
    window.setScene(scene);
    window.showAndWait();
  }

  /**
   * Creates the input fields for affine transformations.
   *
   * @return a VBox containing affine transformation input fields.
   */
  private VBox createAffineTransformationInputs() {
    affineTransformationsContainer = new VBox();
    affineTransformationsContainer.setSpacing(20);
    affineTransformationsContainer.setPadding(new Insets(10));
    affineTransformationsScrollPane = new ScrollPane(affineTransformationsContainer);
    affineTransformationsScrollPane.setFitToWidth(true);
    affineTransformationsScrollPane.setPrefHeight(200);

    addAffineTransformationInputs();

    Button addTransformationButton = new Button("Add transformation");
    addTransformationButton.setOnAction(e -> addAffineTransformationInputs());

    VBox container = new VBox(10, affineTransformationsScrollPane, addTransformationButton);
    container.setSpacing(10);
    container.setVisible(true);

    return container;
  }

  /**
   * Adds a new set of input fields for an affine transformation.
   */
  private void addAffineTransformationInputs() {
    GridPane transformationFields = new GridPane();
    transformationFields.setHgap(10);
    transformationFields.setVgap(8);

    Label matrixLabel = new Label("Matrix:");
    TextField M00Field = new TextField();
    M00Field.setPromptText("M00");
    TextField M01Field = new TextField();
    M01Field.setPromptText("M01");
    TextField M10Field = new TextField();
    M10Field.setPromptText("M10");
    TextField M11Field = new TextField();
    M11Field.setPromptText("M11");

    Label vectorLabel = new Label("Vector:");
    TextField vectorXField = new TextField();
    vectorXField.setPromptText("Vector X");
    TextField vectorYField = new TextField();
    vectorYField.setPromptText("Vector Y");

    transformationFields.add(matrixLabel, 0, 0);
    transformationFields.add(M00Field, 1, 0);
    transformationFields.add(M01Field, 2, 0);
    transformationFields.add(M10Field, 1, 1);
    transformationFields.add(M11Field, 2, 1);

    transformationFields.add(vectorLabel, 0, 2);
    transformationFields.add(vectorXField, 1, 2);
    transformationFields.add(vectorYField, 2, 2);

    affineTransformationsContainer.getChildren().add(transformationFields);
  }

  /**
   * Creates the input fields for Julia transformations.
   *
   * @return a VBox containing Julia transformation input fields.
   */
  private VBox createJuliaTransformationInputs() {
    VBox juliaTransformationInputs = new VBox();
    GridPane transformationFields = new GridPane();
    transformationFields.setHgap(10);
    transformationFields.setVgap(8);

    Label complexLabel = new Label("Complex number:");
    TextField realNumberField = new TextField();
    realNumberField.setPromptText("Real");
    TextField imaginaryNumberField = new TextField();
    imaginaryNumberField.setPromptText("Imaginary");
    Label signLabel = new Label("Sign:");
    TextField signField = new TextField();
    signField.setPromptText("Sign");

    transformationFields.add(complexLabel, 0, 0);
    transformationFields.add(realNumberField, 1, 0);
    transformationFields.add(imaginaryNumberField, 2, 0);
    transformationFields.add(signLabel, 0, 1);
    transformationFields.add(signField, 1, 1);

    juliaTransformationInputs.getChildren().add(transformationFields);
    juliaTransformationInputs.setVisible(false);

    return juliaTransformationInputs;
  }

  /**
   * Displays the fractal detail input box with empty fields.
   */
  public void displayEmpty() {
    presetChoiceBox.setValue("Custom");
    transformChoiceBox.setValue("Affine");
    affineTransformationsContainer.getChildren().clear();
    display(null);
  }

  /**
   * Displays the fractal detail input box with the given description.
   *
   * @param description the chaos game description.
   */
  public void displayWithValues(ChaosGameDescription description) {
    affineTransformationsContainer.getChildren().clear();
    int iterations = getIterations();
    display(description);
    iterationsField.setText(String.valueOf(iterations));
  }

  /**
   * Displays the preset fractal details.
   */
  public void displayPreset() {
    iterationsField.setText("1000000");
    if (chaosGameFileReader.isJuliaTransform(selectedPreset)) {
      transformChoiceBox.setValue("Julia");
    } else {
      transformChoiceBox.setValue("Affine");
    }
  }

  /**
   * Clears all fields and displays the fractal detail input box.
   */
  public void displayNoPreset() {
    minXField.clear();
    minYField.clear();
    maxXField.clear();
    maxYField.clear();
    iterationsField.clear();
    affineTransformationsContainer.getChildren().clear();
  }

  /**
   * Updates the fields with the given description.
   *
   * @param description the chaos game description.
   */
  public void updateFields(ChaosGameDescription description) {
    affineTransformationsContainer.getChildren().clear();

    if (description != null) {
      minXField.setText(String.valueOf(description.getMinCoords().getx0()));
      minYField.setText(String.valueOf(description.getMinCoords().getx1()));
      maxXField.setText(String.valueOf(description.getMaxCoords().getx0()));
      maxYField.setText(String.valueOf(description.getMaxCoords().getx1()));

      if (transformChoiceBox.getValue().equals("Affine")) {
        for (Transform2D transform : description.getTransforms()) {
          if (transform instanceof AffineTransform2D) {
            AffineTransform2D affineTransform = (AffineTransform2D) transform;
            GridPane transformationFields = new GridPane();
            transformationFields.setHgap(10);
            transformationFields.setVgap(8);

            Label matrixLabel = new Label("Matrix:");
            TextField M00Field = new TextField(String.valueOf(affineTransform.getMatrix().getA00()));
            TextField M01Field = new TextField(String.valueOf(affineTransform.getMatrix().getA01()));
            TextField M10Field = new TextField(String.valueOf(affineTransform.getMatrix().getA10()));
            TextField M11Field = new TextField(String.valueOf(affineTransform.getMatrix().getA11()));

            Label vectorLabel = new Label("Vector:");
            TextField vectorXField = new TextField(String.valueOf(affineTransform.getVector().getx0()));
            TextField vectorYField = new TextField(String.valueOf(affineTransform.getVector().getx1()));

            transformationFields.add(matrixLabel, 0, 0);
            transformationFields.add(M00Field, 1, 0);
            transformationFields.add(M01Field, 2, 0);
            transformationFields.add(M10Field, 1, 1);
            transformationFields.add(M11Field, 2, 1);

            transformationFields.add(vectorLabel, 0, 2);
            transformationFields.add(vectorXField, 1, 2);
            transformationFields.add(vectorYField, 2, 2);

            affineTransformationsContainer.getChildren().add(transformationFields);
          }
        }
      } else if (transformChoiceBox.getValue().equals("Julia")) {
        for (Transform2D transform : description.getTransforms()) {
          if (transform instanceof JuliaTransform) {
            JuliaTransform juliaTransform = (JuliaTransform) transform;
            GridPane transformationFields = new GridPane();
            transformationFields.setHgap(10);
            transformationFields.setVgap(8);

            Label complexLabel = new Label("Complex number:");
            TextField realNumberField = new TextField(String.valueOf(juliaTransform.getC().getx0()));
            TextField imaginaryNumberField = new TextField(String.valueOf(juliaTransform.getC().getx1()));
            Label signLabel = new Label("Sign:");
            TextField signField = new TextField(String.valueOf(juliaTransform.getSign()));

            transformationFields.add(complexLabel, 0, 0);
            transformationFields.add(realNumberField, 1, 0);
            transformationFields.add(imaginaryNumberField, 2, 0);
            transformationFields.add(signLabel, 0, 1);
            transformationFields.add(signField, 1, 1);

            juliaTransformationInputs.getChildren().clear();
            juliaTransformationInputs.getChildren().add(transformationFields);
          }
        }
      }
    }
  }

  public Button getCreateButton() {
    return createButton;
  }

  public Button getClearButton() {
    return clearButton;
  }

  public Stage getWindow() {
    return window;
  }

  public String getTransformationChoice() {
    return transformChoiceBox.getValue();
  }

  public double getMinXField() {
    return Double.parseDouble(minXField.getText());
  }

  public double getMinYField() {
    return Double.parseDouble(minYField.getText());
  }

  public double getMaxXField() {
    return Double.parseDouble(maxXField.getText());
  }

  public double getMaxYField() {
    return Double.parseDouble(maxYField.getText());
  }

  public List<Transform2D> getCurrentTransforms() {
    List<Transform2D> transforms = new ArrayList<>();

    if ("Affine".equals(transformChoiceBox.getValue())) {
      for (Node node : affineTransformationsContainer.getChildren()) {
        GridPane gridPane = (GridPane) node;
        TextField M00Field = (TextField) gridPane.getChildren().get(1);
        TextField M01Field = (TextField) gridPane.getChildren().get(2);
        TextField M10Field = (TextField) gridPane.getChildren().get(3);
        TextField M11Field = (TextField) gridPane.getChildren().get(4);
        TextField vectorXField = (TextField) gridPane.getChildren().get(6);
        TextField vectorYField = (TextField) gridPane.getChildren().get(7);

        Matrix2x2 matrix = new Matrix2x2(Double.parseDouble(M00Field.getText()), Double.parseDouble(M01Field.getText()), Double.parseDouble(M10Field.getText()), Double.parseDouble(M11Field.getText()));
        Vector2D vector = new Vector2D(Double.parseDouble(vectorXField.getText()), Double.parseDouble(vectorYField.getText()));

        transforms.add(new AffineTransform2D(matrix, vector));
      }
    } else if ("Julia".equals(transformChoiceBox.getValue())) {
      Complex juliaConstant = getJuliaConstant();
      TextField signField = (TextField) ((GridPane) juliaTransformationInputs.getChildren().get(0)).getChildren().get(4);
      int sign = Integer.parseInt(signField.getText());

      transforms.add(new JuliaTransform(juliaConstant, sign));
    }

    return transforms;
  }

  public Complex getJuliaConstant() {
    GridPane gridPane = (GridPane) juliaTransformationInputs.getChildren().get(0);
    TextField realNumberField = (TextField) gridPane.getChildren().get(1);
    TextField imaginaryNumberField = (TextField) gridPane.getChildren().get(2);

    double real = Double.parseDouble(realNumberField.getText());
    double imaginary = Double.parseDouble(imaginaryNumberField.getText());

    return new Complex(real, imaginary);
  }

  public int getIterations() {
    return Integer.parseInt(iterationsField.getText());
  }
}
