package org.example.view;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * A utility class for displaying a confirmation dialog box.
 */
public class ConfirmBox {
  static boolean answer;

  /**
   * Displays a confirmation dialog box with a given title and message.
   *
   * @param title   the title of the dialog box.
   * @param message the message to be displayed in the dialog box.
   * @return true if "Yes" is clicked, false if "No" is clicked.
   */
  public static boolean display(String title, String message){
    Stage window = new Stage();
    window.setTitle(title);
    Label label = new Label();
    label.setText(message);

    Button yesButton = new Button("Yes");
    Button noButton = new Button("No");

    yesButton.setOnAction(e -> {
      answer = true;
      window.close();
    });

    noButton.setOnAction(e -> {
      answer = false;
      window.close();
    });

    VBox layout = new VBox(10);
    layout.getChildren().addAll(label, yesButton, noButton);
    layout.setAlignment(Pos.CENTER);
    Scene scene = new Scene(layout, 300, 150);
    window.setScene(scene);
    window.showAndWait();

    return answer;
  }
}
