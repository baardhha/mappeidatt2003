package org.example.view;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;


/**
 * A class for managing the main menu view and its elements.
 */
public class MainMenu {
  private Scene scene;
  private Label mainMenuLabel;
  private Button startGameButton;
  private Button exitGameButton;
  private final String STYLESHEET = "Design/Design.css";

  MediaPlayer mediaPlayer;

  public void playMusic() {
    String musicFile = "src/main/resources/Design/Resolution.mp3";
    Media sound = new Media(java.nio.file.Paths.get(musicFile).toUri().toString());
    mediaPlayer = new MediaPlayer(sound);
    mediaPlayer.play();
  }

  /**
   * Constructor for MainMenu.
   *
   * @param primaryStage the primary stage for the main menu.
   */
  public MainMenu(Stage primaryStage) {
    playMusic();
    userInterfaceSetup(primaryStage);
  }

  /**
   * Sets up the user interface for the main menu.
   *
   * @param primaryStage the primary stage for the main menu.
   */
  private void userInterfaceSetup(Stage primaryStage) {
    VBox layout = new VBox(20);

    try {
      Font.loadFont(getClass().getResourceAsStream("src/main/resources/Design/garet/garet/Garet_Book.ttf"), 12);
      Font.loadFont(getClass().getResourceAsStream("src/main/resources/Design/chaos_in_wisconsin/chaos_in_wisconsin/Chaos_in_Wisconsin.ttf"), 12);
    } catch (Exception e) {
      System.out.println("Font loading failed: " + e.getMessage());
    }

    mainMenuLabel = new Label("Chaos Game");
    mainMenuLabel.getStyleClass().add("mainMenuLabel");
    startGameButton = new Button("Start Game");
    startGameButton.getStyleClass().add("mainMenuButton");
    exitGameButton = new Button("Exit Game");
    exitGameButton.getStyleClass().add("mainMenuButton");

    layout.getChildren().addAll(mainMenuLabel, startGameButton, exitGameButton);
    layout.setAlignment(Pos.CENTER);

    scene = new Scene(layout);
    scene.getStylesheets().add(STYLESHEET);

    layout.prefWidthProperty().bind(scene.widthProperty());
    layout.prefHeightProperty().bind(scene.heightProperty());

    primaryStage.setScene(scene);
  }

  public Scene getScene() {
    return scene;
  }

  public Button getStartGameButton() {
    return startGameButton;
  }

  public Button getExitGameButton() {
    return exitGameButton;
  }
}
