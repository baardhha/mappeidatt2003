package org.example.view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * A utility class for displaying an input dialog box.
 */
public class InputBox {
  static String answer;

  /**
   * Displays an input dialog box with a given title and message.
   *
   * @param title   the title of the dialog box.
   * @param message the message to be displayed in the dialog box.
   * @return the text entered by the user.
   */
  public static String display(String title, String message){
    Stage window = new Stage();
    window.setTitle(title);
    Label label = new Label();
    label.setText(message);

    TextField textField = new TextField();

    Button submitButton = new Button("OK");

    submitButton.setOnAction(e -> {
      answer = textField.getText();
      window.close();
    });

    VBox layout = new VBox(10);
    layout.setPadding(new Insets(20));
    layout.getChildren().addAll(label, textField, submitButton);
    layout.setAlignment(Pos.CENTER);
    Scene scene = new Scene(layout, 300, 150);
    window.setScene(scene);
    window.showAndWait();

    return answer;
  }
}
