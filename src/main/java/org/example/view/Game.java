package org.example.view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 * A class for managing the game view and its elements.
 */
public class Game {
  private Scene scene;
  private Button backToMainMenuButton;
  private Button editFractalButton;
  private Button newFractalButton;
  private Button clearButton;
  private Button zoomInButton;
  private Button zoomOutButton;
  private Button saveToFileButton;
  private ImageView fractalImageView;
  private final String STYLESHEET = "Design/Design.css";

  /**
   * Constructor for Game.
   *
   * @param primaryStage the primary stage for the game.
   */
  public Game(Stage primaryStage) {
    userInterfaceSetup(primaryStage);
  }

  /**
   * Sets up the user interface for the game.
   *
   * @param primaryStage the primary stage for the game.
   */
  private void userInterfaceSetup(Stage primaryStage) {
    BorderPane layout = new BorderPane();
    layout.setPadding(new Insets(0, 0, 10, 0));

    try {
      Font.loadFont(getClass().getResourceAsStream("src/main/resources/Design/garet/garet/Garet_Book.ttf"), 12);
    } catch (Exception e) {
      System.out.println("Font loading failed: " + e.getMessage());
    }

    backToMainMenuButton = new Button("Return to main menu");
    newFractalButton = new Button("New Fractal");
    editFractalButton = new Button("Edit Fractal");
    clearButton = new Button("Clear");
    zoomInButton = new Button("Zoom In");
    zoomOutButton = new Button("Zoom Out");
    saveToFileButton = new Button("Save to file");

    backToMainMenuButton.getStyleClass().add("gameButton");
    newFractalButton.getStyleClass().add("gameButton");
    editFractalButton.getStyleClass().add("gameButton");
    clearButton.getStyleClass().add("gameButton");
    zoomInButton.getStyleClass().add("gameButton");
    zoomOutButton.getStyleClass().add("gameButton");
    saveToFileButton.getStyleClass().add("gameButton");

    HBox optionBar = new HBox(10);
    optionBar.getChildren().addAll(newFractalButton, editFractalButton, clearButton, backToMainMenuButton, zoomInButton, zoomOutButton, saveToFileButton);
    optionBar.setAlignment(Pos.CENTER);

    fractalImageView = new ImageView();
    fractalImageView.setPreserveRatio(true);

    layout.setCenter(fractalImageView);
    layout.setBottom(optionBar);

    scene = new Scene(layout);
    scene.getStylesheets().add(STYLESHEET);

    layout.prefWidthProperty().bind(scene.widthProperty());
    layout.prefHeightProperty().bind(scene.heightProperty());

    fractalImageView.fitWidthProperty().bind(scene.widthProperty().subtract(1));
    fractalImageView.fitHeightProperty().bind(scene.heightProperty().subtract(60));

    primaryStage.setScene(scene);
  }

  public Scene getScene() {
    return scene;
  }

  public Button getEditFractalButton() {
    return editFractalButton;
  }

  public Button getNewFractalButton() {
    return newFractalButton;
  }

  public Button getClearButton() {
    return clearButton;
  }

  public Button getBackToMainMenuButton() {
    return backToMainMenuButton;
  }

  public Button getZoomInButton() {
    return zoomInButton;
  }

  public Button getZoomOutButton() {
    return zoomOutButton;
  }

  public Button getSaveToFileButton() {
    return saveToFileButton;
  }

  public ImageView getFractalImageView() {
    return fractalImageView;
  }
}
