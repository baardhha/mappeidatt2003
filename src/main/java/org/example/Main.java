package org.example;

import javafx.application.Application;
import javafx.stage.Stage;
import org.example.controller.ChaosGameController;
import org.example.view.Game;
import org.example.view.MainMenu;

public class Main extends Application {
  @Override
  public void start(Stage primaryStage) {
    MainMenu mainMenu = new MainMenu(primaryStage);
    Game game = new Game(primaryStage);
    new ChaosGameController(mainMenu, game, primaryStage);

    primaryStage.setTitle("Chaos Game");
    primaryStage.setScene(mainMenu.getScene());

    primaryStage.setMaximized(true);
    primaryStage.setMinWidth(800);
    primaryStage.setMinHeight(450);

    primaryStage.show();
  }

  public static void main(String[] args) {
    launch(args);
  }
}