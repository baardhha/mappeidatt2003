package org.example.model;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class for Vector2D.
 * This class contains unit tests for the Vector2D class,
 * including positive and negative test cases.
 */
public class Vector2DTest {

    /**
     * Tests the constructor and the getter methods of Vector2D.
     * Verifies that the coordinates are correctly set and retrieved.
     */
    @Test
    public void testConstructorAndGetters() {
        Vector2D vector = new Vector2D(1.0, 2.0);

        assertEquals(1.0, vector.getx0());
        assertEquals(2.0, vector.getx1());
    }

    /**
     * Tests the add method.
     * Verifies that the addition of two vectors is computed correctly.
     */
    @Test
    public void testAdd() {
        Vector2D vector1 = new Vector2D(1.0, 2.0);
        Vector2D vector2 = new Vector2D(3.0, 4.0);
        Vector2D result = vector1.add(vector2);

        assertEquals(new Vector2D(4.0, 6.0), result);
    }

    /**
     * Tests the add method with negative values.
     * Verifies that the addition of two vectors with negative values is computed correctly.
     */
    @Test
    public void testAddWithNegativeValues() {
        Vector2D vector1 = new Vector2D(-1.0, -2.0);
        Vector2D vector2 = new Vector2D(3.0, 4.0);
        Vector2D result = vector1.add(vector2);

        assertEquals(new Vector2D(2.0, 2.0), result);
    }

    /**
     * Tests the subtract method.
     * Verifies that the subtraction of two vectors is computed correctly.
     */
    @Test
    public void testSubtract() {
        Vector2D vector1 = new Vector2D(5.0, 6.0);
        Vector2D vector2 = new Vector2D(3.0, 2.0);
        Vector2D result = vector1.subtract(vector2);

        assertEquals(new Vector2D(2.0, 4.0), result);
    }

    /**
     * Tests the subtract method with negative values.
     * Verifies that the subtraction of two vectors with negative values is computed correctly.
     */
    @Test
    public void testSubtractWithNegativeValues() {
        Vector2D vector1 = new Vector2D(1.0, 2.0);
        Vector2D vector2 = new Vector2D(-3.0, -4.0);
        Vector2D result = vector1.subtract(vector2);

        assertEquals(new Vector2D(4.0, 6.0), result);
    }

    /**
     * Tests the equals method.
     * Verifies that two vectors with the same coordinates are considered equal.
     */
    @Test
    public void testEquals() {
        Vector2D vector1 = new Vector2D(1.0, 2.0);
        Vector2D vector2 = new Vector2D(1.0, 2.0);
        Vector2D vector3 = new Vector2D(2.0, 3.0);

        assertTrue(vector1.equals(vector2));
        assertFalse(vector1.equals(vector3));
    }

    /**
     * Tests the equals method with a different object type.
     * Verifies that a vector is not equal to an object of a different type.
     */
    @Test
    public void testEqualsWithDifferentObject() {
        Vector2D vector = new Vector2D(1.0, 2.0);
        String notAVector = "Not a Vector";

        assertFalse(vector.equals(notAVector));
    }

    /**
     * Tests the clone method.
     * Verifies that a cloned vector is equal to the original vector but is a different instance.
     */
    @Test
    public void testClone() {
        Vector2D vector = new Vector2D(1.0, 2.0);
        Vector2D clonedVector = vector.copy();

        assertEquals(vector, clonedVector);
        assertNotSame(vector, clonedVector); // Ensure it's a deep copy
    }

    /**
     * Tests the hashCode method.
     * Verifies that two equal vectors have the same hash code.
     */
    @Test
    public void testHashCode() {
        Vector2D vector1 = new Vector2D(1.0, 2.0);
        Vector2D vector2 = new Vector2D(1.0, 2.0);
        Vector2D vector3 = new Vector2D(2.0, 3.0);

        assertEquals(vector1.hashCode(), vector2.hashCode());
        assertNotEquals(vector1.hashCode(), vector3.hashCode());
    }

    /**
     * Tests the toString method.
     * Verifies that the string representation of a vector is correct.
     */
    @Test
    public void testToString() {
        Vector2D vector = new Vector2D(1.0, 2.0);
        String expected = "Vector2D{x0=1.0, x1=2.0}";

        assertEquals(expected, vector.toString());
    }

    /**
     * Tests the add method with a null vector.
     * Verifies that the add method throws a NullPointerException when the input vector is null.
     */
    @Test
    public void testAddNullVector() {
        Vector2D vector1 = new Vector2D(1.0, 2.0);

        assertThrows(NullPointerException.class, () -> {
            vector1.add(null);
        });
    }

    /**
     * Tests the subtract method with a null vector.
     * Verifies that the subtract method throws a NullPointerException when the input vector is null.
     */
    @Test
    public void testSubtractNullVector() {
        Vector2D vector1 = new Vector2D(1.0, 2.0);

        assertThrows(NullPointerException.class, () -> {
            vector1.subtract(null);
        });
    }
}
