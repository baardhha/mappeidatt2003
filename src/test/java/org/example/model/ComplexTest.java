package org.example.model;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class for Complex.
 * This class contains unit tests for the Complex class,
 * including positive and negative test cases.
 */
public class ComplexTest {

    /**
     * Tests the constructor and the getter methods of Complex.
     * Verifies that the real and imaginary parts are correctly set and retrieved.
     */
    @Test
    public void testConstructorAndGetters() {
        Complex complex = new Complex(3.0, 4.0);

        assertEquals(3.0, complex.getx0());
        assertEquals(4.0, complex.getx1());
    }

    /**
     * Tests the sqrt method.
     * Verifies that the square root of the complex number is computed correctly.
     */
    @Test
    public void testSqrt() {
        Complex complex = new Complex(4.0, 0.0);
        Complex result = complex.sqrt();

        assertEquals(new Complex(2.0, 0.0), result);

        Complex complex2 = new Complex(0.0, 4.0);
        Complex result2 = complex2.sqrt();

        assertEquals(new Complex(1.4142135623730951, 1.4142135623730951), result2);
    }

    /**
     * Tests the square method.
     * Verifies that the square of the complex number is computed correctly.
     */
    @Test
    public void testSquare() {
        Complex complex = new Complex(3.0, 4.0);
        Complex result = complex.square();

        assertEquals(new Complex(-7.0, 24.0), result);
    }

    /**
     * Tests the negate method.
     * Verifies that the negation of the complex number is computed correctly.
     */
    @Test
    public void testNegate() {
        Complex complex = new Complex(3.0, 4.0);
        Complex result = complex.negate();

        assertEquals(new Complex(-3.0, -4.0), result);
    }

    /**
     * Tests the magnitude method.
     * Verifies that the magnitude of the complex number is computed correctly.
     */
    @Test
    public void testMagnitude() {
        Complex complex = new Complex(3.0, 4.0);
        double result = complex.magnitude();

        assertEquals(5.0, result);
    }

    /**
     * Tests the add method.
     * Verifies that the addition of the complex number and a vector is computed correctly.
     */
    @Test
    public void testAdd() {
        Complex complex1 = new Complex(3.0, 4.0);
        Vector2D vector = new Vector2D(1.0, 2.0);
        Complex result = complex1.add(vector);

        assertEquals(new Complex(4.0, 6.0), result);
    }

    /**
     * Tests the clone method.
     * Verifies that a cloned complex number is equal to the original complex number but is a different instance.
     */
    @Test
    public void testClone() {
        Complex complex = new Complex(3.0, 4.0);
        Complex clonedComplex = complex.clone();

        assertEquals(complex, clonedComplex);
        assertNotSame(complex, clonedComplex); // Ensure it's a deep copy
    }

    /**
     * Tests the toString method.
     * Verifies that the string representation of a complex number is correct.
     */
    @Test
    public void testToString() {
        Complex complex = new Complex(3.0, 4.0);
        String expected = "Complex{real=3.0, imaginary=4.0}";

        assertEquals(expected, complex.toString());
    }

    /**
     * Tests the sqrt method with a negative number.
     * Verifies that the square root of a negative number is computed correctly.
     */
    @Test
    public void testSqrtNegative() {
        Complex complex = new Complex(-4.0, 0.0);
        Complex result = complex.sqrt();

        assertEquals(new Complex(0.0, 2.0), result);
    }

    /**
     * Tests the add method with a null vector.
     * Verifies that the add method throws a NullPointerException when the input vector is null.
     */
    @Test
    public void testAddNullVector() {
        Complex complex1 = new Complex(3.0, 4.0);

        assertThrows(NullPointerException.class, () -> {
            complex1.add(null);
        });
    }
}
