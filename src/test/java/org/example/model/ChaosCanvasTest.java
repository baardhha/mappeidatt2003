package org.example.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class for ChaosCanvas.
 * This class contains unit tests for the ChaosCanvas class,
 * including positive and negative test cases.
 */
public class ChaosCanvasTest {

    private ChaosCanvas canvas;
    private Vector2D minCoords;
    private Vector2D maxCoords;

    /**
     * Sets up the ChaosCanvas instance before each test.
     * Initializes the min and max coordinates and ChaosCanvas object.
     */
    @BeforeEach
    public void setUp() {
        minCoords = new Vector2D(-2.0, -2.0);
        maxCoords = new Vector2D(2.0, 2.0);
        canvas = new ChaosCanvas(800, 600, minCoords, maxCoords);
    }

    /**
     * Tests the constructor and the getter methods of ChaosCanvas.
     * Verifies that the fields are correctly set and retrieved.
     */
    @Test
    public void testConstructorAndGetters() {
        assertEquals(800, canvas.getWidth());
        assertEquals(600, canvas.getHeight());
        assertEquals(minCoords, canvas.getMinCoords());
        assertEquals(maxCoords, canvas.getMaxCoords());
    }

    /**
     * Tests the getPixel method for a point within the bounds.
     * Verifies that the correct pixel value is retrieved.
     */
    @Test
    public void testGetPixel_InBounds() {
        Vector2D point = new Vector2D(0.0, 0.0);
        assertEquals(0, canvas.getPixel(point));
    }

    /**
     * Tests the getPixel method for a point outside the bounds.
     * Verifies that the method returns 0 for out-of-bounds points.
     */
    @Test
    public void testGetPixel_OutOfBounds() {
        Vector2D point = new Vector2D(3.0, 3.0);
        assertEquals(0, canvas.getPixel(point));
    }

    /**
     * Tests the putPixel method for a point outside the bounds.
     * Verifies that the method does not set any pixel for out-of-bounds points.
     */
    @Test
    public void testPutPixel_OutOfBounds() {
        Vector2D point = new Vector2D(3.0, 3.0);
        canvas.putPixel(point, 1.0, new Vector2D(0.0, 0.0));
        assertEquals(0, canvas.getPixel(point));
    }

    /**
     * Tests the updateTransform method.
     * Verifies that the transformation is correctly updated with new coordinates.
     */
    @Test
    public void testUpdateTransform() {
        Vector2D newMinCoords = new Vector2D(-1.0, -1.0);
        Vector2D newMaxCoords = new Vector2D(1.0, 1.0);
        canvas.updateTransform(newMinCoords, newMaxCoords);
        assertEquals(newMinCoords, canvas.getMinCoords());
        assertEquals(newMaxCoords, canvas.getMaxCoords());
    }

    /**
     * Tests the clear method.
     * Verifies that all pixels on the canvas are set to 0.
     */
    @Test
    public void testClear() {
        Vector2D point = new Vector2D(0.0, 0.0);
        canvas.putPixel(point, 1.0, new Vector2D(0.0, 0.0));
        canvas.clear();
        assertEquals(0, canvas.getPixel(point));
    }

    /**
     * Tests the toWritableImage method.
     * Verifies that the WritableImage is correctly created from the canvas.
     */
    @Test
    public void testToWritableImage() {
        assertNotNull(canvas.toWritableImage());
    }

    /**
     * Tests the printCanvas method.
     * Verifies that the canvas is printed to the console without exceptions.
     */
    @Test
    public void testPrintCanvas() {
        canvas.printCanvas();
        // No assertions needed as we're just checking for exceptions and runtime errors
    }

    /**
     * Tests the createTransform method with invalid coordinates.
     * Verifies that the method throws an IllegalArgumentException.
     */
    @Test
    public void testCreateTransformWithInvalidCoordinates() {
        assertThrows(IllegalArgumentException.class, () -> {
            new ChaosCanvas(800, 600, new Vector2D(2.0, 2.0), new Vector2D(-2.0, -2.0));
        });
    }

    /**
     * Tests the putPixel method with null point.
     * Verifies that the method throws a NullPointerException.
     */
    @Test
    public void testPutPixelWithNullPoint() {
        assertThrows(NullPointerException.class, () -> {
            canvas.putPixel(null, 1.0, new Vector2D(0.0, 0.0));
        });
    }
}
