package org.example.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import java.util.Arrays;
import java.util.List;

/**
 * Test class for ChaosGameDescription.
 * This class contains unit tests for the ChaosGameDescription class,
 * including positive and negative test cases.
 */
public class ChaosGameDescriptionTest {

    private Vector2D minCoords;
    private Vector2D maxCoords;
    private List<Transform2D> transforms;
    private Complex juliaConstant;
    private ChaosGameDescription description;

    /**
     * Sets up the ChaosGameDescription instance before each test.
     * Initializes the min and max coordinates, transforms, and ChaosGameDescription object.
     */
    @BeforeEach
    public void setUp() {
        minCoords = new Vector2D(-2.0, -2.0);
        maxCoords = new Vector2D(2.0, 2.0);

        Transform2D transform1 = new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(1, 1));
        Transform2D transform2 = new JuliaTransform(new Complex(0.355, 0.355), 1);
        transforms = Arrays.asList(transform1, transform2);

        juliaConstant = new Complex(0.355, 0.355);
        description = new ChaosGameDescription(transforms, minCoords, maxCoords, juliaConstant);
    }

    /**
     * Tests the constructor with Julia constant.
     * Verifies that the fields are correctly set and retrieved.
     */
    @Test
    public void testConstructorWithJuliaConstant() {
        ChaosGameDescription desc = new ChaosGameDescription(transforms, minCoords, maxCoords, juliaConstant);
        assertEquals(transforms, desc.getTransforms());
        assertEquals(minCoords, desc.getMinCoords());
        assertEquals(maxCoords, desc.getMaxCoords());
        assertEquals(juliaConstant, desc.juliaConstant);
    }

    /**
     * Tests the constructor without Julia constant.
     * Verifies that the fields are correctly set and retrieved, and Julia constant is null.
     */
    @Test
    public void testConstructorWithoutJuliaConstant() {
        ChaosGameDescription desc = new ChaosGameDescription(transforms, minCoords, maxCoords);
        assertEquals(transforms, desc.getTransforms());
        assertEquals(minCoords, desc.getMinCoords());
        assertEquals(maxCoords, desc.getMaxCoords());
        assertNull(desc.juliaConstant);
    }

    /**
     * Tests the getTransforms method.
     * Verifies that the transforms list is correctly retrieved.
     */
    @Test
    public void testGetTransforms() {
        assertEquals(transforms, description.getTransforms());
    }

    /**
     * Tests the getMinCoords method.
     * Verifies that the minimum coordinates are correctly retrieved.
     */
    @Test
    public void testGetMinCoords() {
        assertEquals(minCoords, description.getMinCoords());
    }

    /**
     * Tests the getMaxCoords method.
     * Verifies that the maximum coordinates are correctly retrieved.
     */
    @Test
    public void testGetMaxCoords() {
        assertEquals(maxCoords, description.getMaxCoords());
    }

    /**
     * Tests the setTransforms method.
     * Verifies that the transforms list is correctly updated.
     */
    @Test
    public void testSetTransforms() {
        Transform2D transform3 = new AffineTransform2D(new Matrix2x2(1, 0, 0, 1), new Vector2D(0, 0));
        List<Transform2D> newTransforms = Arrays.asList(transform3);
        description.setTransforms(newTransforms);
        assertEquals(newTransforms, description.getTransforms());
    }

    /**
     * Tests the setJuliaConstant method.
     * Verifies that the Julia constant is correctly updated.
     */
    @Test
    public void testSetJuliaConstant() {
        Complex newJuliaConstant = new Complex(0.5, 0.5);
        description.setJuliaConstant(newJuliaConstant);
        assertEquals(newJuliaConstant, description.juliaConstant);
    }

    /**
     * Tests the clone method.
     * Verifies that a cloned ChaosGameDescription is equal to the original but is a different instance.
     */
    @Test
    public void testClone() {
        ChaosGameDescription clonedDescription = description.copy();
        assertEquals(description.getTransforms().size(), clonedDescription.getTransforms().size());
        assertEquals(description.getMinCoords(), clonedDescription.getMinCoords());
        assertEquals(description.getMaxCoords(), clonedDescription.getMaxCoords());
        assertEquals(description.juliaConstant, clonedDescription.juliaConstant);

        // Ensure deep copy
        assertNotSame(description.getTransforms(), clonedDescription.getTransforms());
        assertNotSame(description.getMinCoords(), clonedDescription.getMinCoords());
        assertNotSame(description.getMaxCoords(), clonedDescription.getMaxCoords());
        if (description.juliaConstant != null) {
            assertNotSame(description.juliaConstant, clonedDescription.juliaConstant);
        }
    }
}
