package org.example.model;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class for Matrix2x2.
 * This class contains unit tests for the Matrix2x2 class,
 * including positive and negative test cases.
 */
public class Matrix2x2Test {

    /**
     * Tests the constructor and the getter methods of Matrix2x2.
     * Verifies that the elements are correctly set and retrieved.
     */
    @Test
    public void testConstructorAndGetters() {
        Matrix2x2 matrix = new Matrix2x2(1.0, 2.0, 3.0, 4.0);

        assertEquals(1.0, matrix.getA00());
        assertEquals(2.0, matrix.getA01());
        assertEquals(3.0, matrix.getA10());
        assertEquals(4.0, matrix.getA11());
    }

    /**
     * Tests the multiply method with a vector.
     * Verifies that the matrix-vector multiplication is computed correctly.
     */
    @Test
    public void testMultiplyVector() {
        Matrix2x2 matrix = new Matrix2x2(1.0, 2.0, 3.0, 4.0);
        Vector2D vector = new Vector2D(1.0, 1.0);
        Vector2D result = matrix.multiply(vector);

        assertEquals(new Vector2D(3.0, 7.0), result);
    }

    /**
     * Tests the multiply method with another matrix.
     * Verifies that the matrix-matrix multiplication is computed correctly.
     */
    @Test
    public void testMultiplyMatrix() {
        Matrix2x2 matrix1 = new Matrix2x2(1.0, 2.0, 3.0, 4.0);
        Matrix2x2 matrix2 = new Matrix2x2(2.0, 0.0, 1.0, 2.0);
        Matrix2x2 result = matrix1.multiply(matrix2);

        Matrix2x2 expected = new Matrix2x2(4.0, 4.0, 10.0, 8.0);
        assertEquals(expected, result);
    }

    /**
     * Tests the clone method.
     * Verifies that a cloned matrix is equal to the original matrix but is a different instance.
     */
    @Test
    public void testClone() {
        Matrix2x2 matrix = new Matrix2x2(1.0, 2.0, 3.0, 4.0);
        Matrix2x2 clonedMatrix = matrix.copy();

        assertEquals(matrix, clonedMatrix);
        assertNotSame(matrix, clonedMatrix); // Ensure it's a deep copy
    }

    /**
     * Tests the toString method.
     * Verifies that the string representation of a matrix is correct.
     */
    @Test
    public void testToString() {
        Matrix2x2 matrix = new Matrix2x2(1.0, 2.0, 3.0, 4.0);
        String expected = "Matrix2x2{a00=1.0, a01=2.0, a10=3.0, a11=4.0}";

        assertEquals(expected, matrix.toString());
    }

    /**
     * Tests the multiply method with a null vector.
     * Verifies that the multiply method throws a NullPointerException when the input vector is null.
     */
    @Test
    public void testMultiplyNullVector() {
        Matrix2x2 matrix = new Matrix2x2(1.0, 2.0, 3.0, 4.0);

        assertThrows(NullPointerException.class, () -> {
            matrix.multiply((Vector2D) null);
        });
    }

    /**
     * Tests the multiply method with a null matrix.
     * Verifies that the multiply method throws a NullPointerException when the input matrix is null.
     */
    @Test
    public void testMultiplyNullMatrix() {
        Matrix2x2 matrix = new Matrix2x2(1.0, 2.0, 3.0, 4.0);

        assertThrows(NullPointerException.class, () -> {
            matrix.multiply((Matrix2x2) null);
        });
    }

    /**
     * Tests the equals method with different matrices.
     * Verifies that two matrices with different elements are not considered equal.
     */
    @Test
    public void testNotEquals() {
        Matrix2x2 matrix1 = new Matrix2x2(1.0, 2.0, 3.0, 4.0);
        Matrix2x2 matrix2 = new Matrix2x2(2.0, 3.0, 4.0, 5.0);

        assertFalse(matrix1.equals(matrix2));
    }

    /**
     * Tests the equals method with a different object type.
     * Verifies that a matrix is not equal to an object of a different type.
     */
    @Test
    public void testEqualsWithDifferentObject() {
        Matrix2x2 matrix = new Matrix2x2(1.0, 2.0, 3.0, 4.0);
        String notAMatrix = "Not a Matrix";

        assertFalse(matrix.equals(notAMatrix));
    }
}
