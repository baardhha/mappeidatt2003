package org.example.model;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class for JuliaTransform.
 * This class contains unit tests for the JuliaTransform class,
 * including positive and negative test cases.
 */
public class JuliaTransformTest {

    /**
     * Tests the constructor and the getter methods of JuliaTransform.
     * Verifies that the complex constant and sign are correctly set and retrieved.
     */
    @Test
    public void testConstructorAndGetters() {
        Complex c = new Complex(1.0, 2.0);
        int sign = 1;
        JuliaTransform transform = new JuliaTransform(c, sign);

        assertEquals(c, transform.getC());
        assertEquals(sign, transform.getSign());
    }

    /**
     * Tests the transform method with a point that does not escape.
     * Verifies that the point is correctly transformed.
     */
    @Test
    public void testTransform() {
        Complex c = new Complex(0.355, 0.355);
        JuliaTransform transform = new JuliaTransform(c, 1);
        Complex point = new Complex(0.0, 0.0);
        Vector2D result = transform.transform(point);

        assertEquals(new Complex(0.355, 0.355), result);
    }

    /**
     * Tests the transform method with a point that escapes.
     * Verifies that the method returns null for points that escape.
     */
    @Test
    public void testTransformEscape() {
        Complex c = new Complex(0.355, 0.355);
        JuliaTransform transform = new JuliaTransform(c, 1);
        Complex point = new Complex(10.0, 10.0);
        Vector2D result = transform.transform(point);

        assertNull(result);
    }

    /**
     * Tests the transform method with a negative sign.
     * Verifies that the point is correctly transformed using the negative sign.
     */
    @Test
    public void testTransformWithNegativeSign() {
        Complex c = new Complex(0.355, 0.355);
        JuliaTransform transform = new JuliaTransform(c, -1);
        Complex point = new Complex(0.0, 0.0);
        Vector2D result = transform.transform(point);

        assertEquals(new Complex(-0.355, -0.355), result);
    }

    /**
     * Tests the transform method with a non-complex point.
     * Verifies that the method throws an IllegalArgumentException when the input point is not a Complex object.
     */
    @Test
    public void testTransformWithNonComplex() {
        Complex c = new Complex(0.355, 0.355);
        JuliaTransform transform = new JuliaTransform(c, 1);
        Vector2D point = new Vector2D(1.0, 2.0);

        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            transform.transform(point);
        });

        String expectedMessage = "Expected Complex type but received Vector2D";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    /**
     * Tests the clone method.
     * Verifies that a cloned JuliaTransform is equal to the original transform but is a different instance.
     */
    @Test
    public void testClone() {
        Complex c = new Complex(0.355, 0.355);
        JuliaTransform transform = new JuliaTransform(c, 1);
        JuliaTransform clonedTransform = (JuliaTransform) transform.copy();

        assertEquals(transform.getC(), clonedTransform.getC());
        assertEquals(transform.getSign(), clonedTransform.getSign());
        assertNotSame(transform, clonedTransform); // Ensure it's a deep copy
    }
}