package org.example.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ChaosGameFileReaderTest {

    private ChaosGameFileReader chaosGameFileReader;

    private String affineThreeTransforms = "src/test/resources/testFiles/readTestAffineThreeTransforms.txt";
    private String affineFourTransforms = "src/test/resources/testFiles/readTestAffineFourTransforms.txt";
    private String juliaTransform = "src/test/resources/testFiles/readTestJulia.txt";
    private String filePath = "src/test/resources/testFiles";

    @BeforeEach
    public void setUp() {
        chaosGameFileReader = new ChaosGameFileReader();
    }

    /**
     * Test reading affine data with three transforms.
     */
    @Test
    public void testReadAffineDataThreeTransforms() {
        chaosGameFileReader.readFromFile(affineThreeTransforms);

        Vector2D minCoords = chaosGameFileReader.getMinCoords();
        Vector2D maxCoords = chaosGameFileReader.getMaxCoords();
        List<Transform2D> transforms = chaosGameFileReader.getTransforms();
        boolean isJuliaTransform = chaosGameFileReader.isJuliaTransform(affineThreeTransforms);

        assertNotNull(minCoords);
        assertNotNull(maxCoords);
        assertEquals(3, transforms.size());
        assertFalse(isJuliaTransform);

        assertEquals(0.0, minCoords.getx0());
        assertEquals(0.0, minCoords.getx1());
        assertEquals(1.0, maxCoords.getx0());
        assertEquals(1.0, maxCoords.getx1());
    }

    @Test
    public void negativeTestReadAffineDataThreeTransforms() {
        chaosGameFileReader.readFromFile(affineThreeTransforms);

        Vector2D minCoords = chaosGameFileReader.getMinCoords();
        Vector2D maxCoords = chaosGameFileReader.getMaxCoords();
        List<Transform2D> transforms = chaosGameFileReader.getTransforms();
        boolean isJuliaTransform = chaosGameFileReader.isJuliaTransform(affineThreeTransforms);

        assertNotNull(minCoords);
        assertNotNull(maxCoords);
        assertNotEquals(4, transforms.size());
        assertFalse(isJuliaTransform);

        assertNotEquals(0.1, minCoords.getx0());
        assertNotEquals(0.1, minCoords.getx1());
        assertNotEquals(0.1, maxCoords.getx0());
        assertNotEquals(0.1, maxCoords.getx1());
    }

    /**
     * Test reading affine data with four transforms.
     */
    @Test
    public void testReadAffineDataFourTransforms() {
        chaosGameFileReader.readFromFile(affineFourTransforms);

        Vector2D minCoords = chaosGameFileReader.getMinCoords();
        Vector2D maxCoords = chaosGameFileReader.getMaxCoords();
        List<Transform2D> transforms = chaosGameFileReader.getTransforms();
        boolean isJuliaTransform = chaosGameFileReader.isJuliaTransform(affineFourTransforms);

        assertNotNull(minCoords);
        assertNotNull(maxCoords);
        assertEquals(4, transforms.size());
        assertFalse(isJuliaTransform);

        assertEquals(-2.65, minCoords.getx0());
        assertEquals(0.0, minCoords.getx1());
        assertEquals(2.65, maxCoords.getx0());
        assertEquals(10.0, maxCoords.getx1());
    }

    /**
     * Test reading Julia data.
     */
    @Test
    public void testReadJuliaData() {
        chaosGameFileReader.readFromFile(juliaTransform);

        Vector2D minCoords = chaosGameFileReader.getMinCoords();
        Vector2D maxCoords = chaosGameFileReader.getMaxCoords();
        List<Transform2D> transforms = chaosGameFileReader.getTransforms();
        boolean isJuliaTransform = chaosGameFileReader.isJuliaTransform(juliaTransform);

        assertNotNull(minCoords);
        assertNotNull(maxCoords);
        assertEquals(1, transforms.size());
        assertTrue(isJuliaTransform);

        assertEquals(-1.6, minCoords.getx0());
        assertEquals(-1, minCoords.getx1());
        assertEquals(1.6, maxCoords.getx0());
        assertEquals(1, maxCoords.getx1());
    }

    @Test
    public void negativeTestReadFromFile() {
        assertDoesNotThrow(() -> chaosGameFileReader.readFromFile("src/test/resources/testFiles/invalid.txt"));
    }

    @Test
    public void testReadFromFile() {
        assertDoesNotThrow(() -> chaosGameFileReader.readFromFile("src/test/resources/testFiles/readTestAffineThreeTransforms.txt"));
    }
}
