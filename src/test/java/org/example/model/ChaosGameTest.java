package org.example.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import java.util.Arrays;
import java.util.List;

/**
 * Test class for ChaosGame.
 * This class contains unit tests for the ChaosGame class,
 * including positive and negative test cases.
 */
public class ChaosGameTest {

    private ChaosCanvas canvas;
    private ChaosGameDescription description;
    private ChaosGame chaosGame;

    /**
     * Sets up the ChaosGame instance before each test.
     * Initializes the canvas, description, and ChaosGame object.
     */
    @BeforeEach
    public void setUp() {
        Vector2D minCoords = new Vector2D(-2.0, -2.0);
        Vector2D maxCoords = new Vector2D(2.0, 2.0);
        canvas = new ChaosCanvas(800, 600, minCoords, maxCoords);

        Transform2D transform1 = new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(1, 1));
        Transform2D transform2 = new JuliaTransform(new Complex(0.355, 0.355), 1);
        List<Transform2D> transforms = Arrays.asList(transform1, transform2);
        Complex complex = new Complex(0.0, 0.0); // Assuming a Complex object is needed

        description = new ChaosGameDescription(transforms, minCoords, maxCoords, complex);

        chaosGame = new ChaosGame(canvas, description);
    }

    /**
     * Tests the constructor and getter methods of ChaosGame.
     * Verifies that the canvas is correctly set and retrieved.
     */
    @Test
    public void testConstructorAndGetters() {
        assertEquals(canvas, chaosGame.getCanvas());
        assertNotNull(chaosGame.getCanvas());
    }

    /**
     * Tests the private method isOutOfBounds using reflection.
     * Verifies that points within and outside the bounds are correctly identified.
     */
    @Test
    public void testIsOutOfBounds() {
        Vector2D inBoundsPoint = new Vector2D(0.0, 0.0);
        Vector2D outOfBoundsPoint = new Vector2D(3.0, 3.0);

        // Use reflection to access the private method
        try {
            java.lang.reflect.Method method = ChaosGame.class.getDeclaredMethod("isOutOfBounds", Vector2D.class);
            method.setAccessible(true);

            boolean resultInBounds = (boolean) method.invoke(chaosGame, inBoundsPoint);
            boolean resultOutOfBounds = (boolean) method.invoke(chaosGame, outOfBoundsPoint);

            assertFalse(resultInBounds, "Point should be in bounds.");
            assertTrue(resultOutOfBounds, "Point should be out of bounds.");
        } catch (Exception e) {
            fail("Failed to invoke isOutOfBounds method: " + e.getMessage());
        }
    }

    /**
     * Tests the runSteps method with zero steps.
     * Verifies that the method handles zero steps gracefully.
     */
    @Test
    public void testRunSteps_ZeroSteps() {
        chaosGame.runSteps(0, 1.0, new Vector2D(0.0, 0.0));
        // No assertions needed as we're just checking for exceptions and runtime errors
    }

    /**
     * Tests the runSteps method with negative steps.
     * Verifies that the method handles negative steps without running any iterations.
     */
    @Test
    public void testRunSteps_NegativeSteps() {
        chaosGame.runSteps(-10, 1.0, new Vector2D(0.0, 0.0));
        // No assertions needed as we're just checking for exceptions and runtime errors
    }

    /**
     * Tests the scaleTransforms method.
     * Verifies that all affine transformations are correctly scaled.
     */
    @Test
    public void testScaleTransforms() {
        chaosGame.scaleTransforms(2.0);

        for (Transform2D transform : description.getTransforms()) {
            if (transform instanceof AffineTransform2D) {
                AffineTransform2D affineTransform = (AffineTransform2D) transform;
                assertEquals(new Matrix2x2(1.0, 0.0, 0.0, 1.0), affineTransform.getMatrix());
            }
        }
    }

    /**
     * Tests the printCanvas method.
     * Verifies that the canvas is printed to the console without exceptions.
     */

    @Test
    public void testPrintCanvas() {
        chaosGame.printCanvas();
        // No assertions needed as we're just checking for exceptions and runtime errors
    }
}
