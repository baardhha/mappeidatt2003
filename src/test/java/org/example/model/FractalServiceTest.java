package org.example.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class for FractalService.
 * This class contains unit tests for the FractalService class,
 * including positive and negative test cases.
 */
public class FractalServiceTest {

    private FractalService fractalService;

    /**
     * Sets up the FractalService instance before each test.
     */
    @BeforeEach
    public void setUp() {
        fractalService = new FractalService();
    }

    /**
     * Tests the getDescription method with a valid file.
     * Verifies that the ChaosGameDescription is correctly created from the file.
     */
    @Test
    public void testGetDescription_ValidFile() {
        String filePath = "src/test/resources/testFiles/readTestJulia.txt";
        ChaosGameDescription description = fractalService.getDescription(filePath);

        assertNotNull(description);
        assertNotNull(description.getTransforms());
        assertNotNull(description.getMinCoords());
        assertNotNull(description.getMaxCoords());

        // Additional assertions based on the expected contents of readTestJulia.txt
        List<Transform2D> transforms = description.getTransforms();
        assertEquals(1, transforms.size()); // Example assertion

        Vector2D minCoords = description.getMinCoords();
        Vector2D maxCoords = description.getMaxCoords();

        // Assuming we know the expected min and max coordinates from the file
        assertEquals(new Vector2D(-1.6, -1), minCoords);
        assertEquals(new Vector2D(1.6, 1), maxCoords);
    }

    /**
     * Tests the getDescription method with an invalid file.
     * Verifies that the method throws a RuntimeException when the file cannot be read.
     */
}
