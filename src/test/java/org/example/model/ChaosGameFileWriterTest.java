package org.example.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class ChaosGameFileWriterTest {

    private ChaosGameFileWriter chaosGameFileWriter;
    private final String filePathAffineThreeTransforms = "src/test/resources/testFiles/writeTestAffineThreeTransforms.txt";
    private final String filePathAffineFourTransforms = "src/test/resources/testFiles/writeTestAffineFourTransforms.txt";
    private final String filePathJulia = "src/test/resources/testFiles/writeTestJulia.txt";

    @BeforeEach
    public void setUp() throws IOException {
        chaosGameFileWriter = new ChaosGameFileWriter();
    }

    @AfterEach
    public void tearDown() {
        try {
            Files.deleteIfExists(new File(filePathAffineThreeTransforms).toPath());
            Files.deleteIfExists(new File(filePathAffineFourTransforms).toPath());
            Files.deleteIfExists(new File(filePathJulia).toPath());
        } catch (IOException e) {
            System.err.println("Error deleting test files" + e.getMessage());
        }
    }

    @Test
    public void testWriteAffineDataToFileThreeTransforms() {
        Vector2D minCoords = new Vector2D(0.0, 0.0);
        Vector2D maxCoords = new Vector2D(1.0, 1.0);
        List<Transform2D> transforms = List.of(
                new AffineTransform2D(new Matrix2x2(0.5, 0.0, 0.0, 0.5), new Vector2D(0.0, 0.0)),
                new AffineTransform2D(new Matrix2x2(0.5, 0.0, 0.0, 0.5), new Vector2D(0.5, 0.0)),
                new AffineTransform2D(new Matrix2x2(0.5, 0.0, 0.0, 0.5), new Vector2D(0.25, 0.5))
        );

        chaosGameFileWriter.writeAffineDataToFile(filePathAffineThreeTransforms, minCoords, maxCoords, transforms);

        try (BufferedReader reader = new BufferedReader(new FileReader(filePathAffineThreeTransforms))) {
            assertEquals("Affine2D", reader.readLine());
            assertEquals("0.0, 0.0", reader.readLine());
            assertEquals("1.0, 1.0", reader.readLine());
            assertEquals("0.5, 0.0, 0.0, 0.5, 0.0, 0.0", reader.readLine());
            assertEquals("0.5, 0.0, 0.0, 0.5, 0.5, 0.0", reader.readLine());
            assertEquals("0.5, 0.0, 0.0, 0.5, 0.25, 0.5", reader.readLine());
        } catch (IOException e) {
            System.err.println("Error reading from file: " + filePathAffineThreeTransforms);
        }
    }

    @Test
    public void testWriteAffineDataToFileFourTransforms() {
        Vector2D minCoords = new Vector2D(0.0, 0.0);
        Vector2D maxCoords = new Vector2D(1.0, 1.0);
        List<Transform2D> transforms = List.of(
                new AffineTransform2D(new Matrix2x2(0.5, 0.0, 0.0, 0.5), new Vector2D(0.0, 0.0)),
                new AffineTransform2D(new Matrix2x2(0.5, 0.0, 0.0, 0.5), new Vector2D(0.5, 0.0)),
                new AffineTransform2D(new Matrix2x2(0.5, 0.0, 0.0, 0.5), new Vector2D(0.25, 0.5)),
                new AffineTransform2D(new Matrix2x2(0.5, 0.0, 0.0, 0.5), new Vector2D(0.75, 0.5))
        );

        chaosGameFileWriter.writeAffineDataToFile(filePathAffineFourTransforms, minCoords, maxCoords, transforms);

        try (BufferedReader reader = new BufferedReader(new FileReader(filePathAffineFourTransforms))) {
            assertEquals("Affine2D", reader.readLine());
            assertEquals("0.0, 0.0", reader.readLine());
            assertEquals("1.0, 1.0", reader.readLine());
            assertEquals("0.5, 0.0, 0.0, 0.5, 0.0, 0.0", reader.readLine());
            assertEquals("0.5, 0.0, 0.0, 0.5, 0.5, 0.0", reader.readLine());
            assertEquals("0.5, 0.0, 0.0, 0.5, 0.25, 0.5", reader.readLine());
            assertEquals("0.5, 0.0, 0.0, 0.5, 0.75, 0.5", reader.readLine());
        } catch (IOException e) {
            System.err.println("Error reading from file: " + filePathAffineFourTransforms);
        }
    }

    @Test
    public void testWriteJuliaDataToFile() {
        Vector2D minCoords = new Vector2D(-1.6, -1.0);
        Vector2D maxCoords = new Vector2D(1.6, 1.0);
        Complex juliaConstant = new Complex(-0.4, 0.6);

        chaosGameFileWriter.writeJuliaDataToFile(filePathJulia, minCoords, maxCoords, juliaConstant);

        try (BufferedReader reader = new BufferedReader(new FileReader(filePathJulia))) {
            assertEquals("Julia", reader.readLine());
            assertEquals("-1.6, -1.0", reader.readLine());
            assertEquals("1.6, 1.0", reader.readLine());
            assertEquals("-0.4, 0.6", reader.readLine());
        } catch (IOException e) {
            System.err.println("Error reading from file: " + filePathJulia);
        }
    }
}
